﻿namespace Dictionary
{
    partial class Form3_3_3_3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            timer1 = new System.Windows.Forms.Timer(components);
            label1 = new Label();
            btnSub = new Button();
            groupBox1 = new GroupBox();
            groupBox8 = new GroupBox();
            label2 = new Label();
            label6 = new Label();
            label7 = new Label();
            label8 = new Label();
            groupBox7 = new GroupBox();
            label9 = new Label();
            btnEnter = new Button();
            groupBox6 = new GroupBox();
            label10 = new Label();
            label5 = new Label();
            groupBox2 = new GroupBox();
            groupBox3 = new GroupBox();
            pictureBox1 = new PictureBox();
            txtQuestion = new TextBox();
            groupBox4 = new GroupBox();
            groupBox5 = new GroupBox();
            button61 = new Button();
            button62 = new Button();
            button63 = new Button();
            button64 = new Button();
            button65 = new Button();
            button66 = new Button();
            button67 = new Button();
            button68 = new Button();
            button69 = new Button();
            button70 = new Button();
            button71 = new Button();
            button72 = new Button();
            button73 = new Button();
            button74 = new Button();
            button75 = new Button();
            button76 = new Button();
            button77 = new Button();
            button78 = new Button();
            button79 = new Button();
            button80 = new Button();
            button1 = new Button();
            button42 = new Button();
            button43 = new Button();
            button44 = new Button();
            button45 = new Button();
            button46 = new Button();
            button47 = new Button();
            button48 = new Button();
            button49 = new Button();
            button50 = new Button();
            button51 = new Button();
            button52 = new Button();
            button53 = new Button();
            button54 = new Button();
            button55 = new Button();
            button56 = new Button();
            button57 = new Button();
            button58 = new Button();
            button59 = new Button();
            button60 = new Button();
            button22 = new Button();
            button23 = new Button();
            button24 = new Button();
            button25 = new Button();
            button26 = new Button();
            button27 = new Button();
            button28 = new Button();
            button29 = new Button();
            button30 = new Button();
            button31 = new Button();
            button32 = new Button();
            button33 = new Button();
            button34 = new Button();
            button35 = new Button();
            button36 = new Button();
            button37 = new Button();
            button38 = new Button();
            button39 = new Button();
            button40 = new Button();
            button41 = new Button();
            button18 = new Button();
            button19 = new Button();
            button20 = new Button();
            button21 = new Button();
            button14 = new Button();
            button15 = new Button();
            button16 = new Button();
            button17 = new Button();
            button10 = new Button();
            button11 = new Button();
            button12 = new Button();
            button13 = new Button();
            button6 = new Button();
            button7 = new Button();
            button8 = new Button();
            button9 = new Button();
            button5 = new Button();
            button4 = new Button();
            button3 = new Button();
            button2 = new Button();
            label4 = new Label();
            label3 = new Label();
            groupBox1.SuspendLayout();
            groupBox8.SuspendLayout();
            groupBox7.SuspendLayout();
            groupBox6.SuspendLayout();
            groupBox2.SuspendLayout();
            groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).BeginInit();
            groupBox5.SuspendLayout();
            SuspendLayout();
            // 
            // timer1
            // 
            timer1.Interval = 10000;
            timer1.Tick += timer1_Tick;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 24F, FontStyle.Regular, GraphicsUnit.Point);
            label1.Location = new Point(115, 27);
            label1.Name = "label1";
            label1.Size = new Size(155, 65);
            label1.TabIndex = 0;
            label1.Text = "label1";
            // 
            // btnSub
            // 
            btnSub.Enabled = false;
            btnSub.Location = new Point(624, 30);
            btnSub.Name = "btnSub";
            btnSub.Size = new Size(117, 35);
            btnSub.TabIndex = 1;
            btnSub.Text = "Submit";
            btnSub.UseVisualStyleBackColor = true;
            btnSub.Click += btnSub_Click;
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(groupBox8);
            groupBox1.Controls.Add(groupBox7);
            groupBox1.Controls.Add(groupBox6);
            groupBox1.Controls.Add(label5);
            groupBox1.Location = new Point(12, 12);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new Size(1224, 150);
            groupBox1.TabIndex = 2;
            groupBox1.TabStop = false;
            // 
            // groupBox8
            // 
            groupBox8.Controls.Add(label2);
            groupBox8.Controls.Add(label6);
            groupBox8.Controls.Add(label7);
            groupBox8.Controls.Add(label8);
            groupBox8.Location = new Point(753, 22);
            groupBox8.Name = "groupBox8";
            groupBox8.Size = new Size(465, 112);
            groupBox8.TabIndex = 8;
            groupBox8.TabStop = false;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Segoe UI", 28F, FontStyle.Bold, GraphicsUnit.Point);
            label2.Location = new Point(6, 24);
            label2.Name = "label2";
            label2.Size = new Size(174, 74);
            label2.TabIndex = 1;
            label2.Text = "HSK3";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new Font("Segoe UI", 11F, FontStyle.Regular, GraphicsUnit.Point);
            label6.Location = new Point(186, 19);
            label6.Name = "label6";
            label6.Size = new Size(129, 30);
            label6.TabIndex = 3;
            label6.Text = "听力：40题";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Font = new Font("Segoe UI", 11F, FontStyle.Regular, GraphicsUnit.Point);
            label7.Location = new Point(186, 49);
            label7.Name = "label7";
            label7.Size = new Size(129, 30);
            label7.TabIndex = 4;
            label7.Text = "阅读：40题";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Font = new Font("Segoe UI", 11F, FontStyle.Regular, GraphicsUnit.Point);
            label8.Location = new Point(186, 79);
            label8.Name = "label8";
            label8.Size = new Size(158, 30);
            label8.TabIndex = 5;
            label8.Text = "时间：90 分钟";
            // 
            // groupBox7
            // 
            groupBox7.Controls.Add(label9);
            groupBox7.Controls.Add(btnEnter);
            groupBox7.Location = new Point(323, 22);
            groupBox7.Name = "groupBox7";
            groupBox7.Size = new Size(424, 112);
            groupBox7.TabIndex = 7;
            groupBox7.TabStop = false;
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Location = new Point(40, 27);
            label9.Name = "label9";
            label9.Size = new Size(284, 25);
            label9.TabIndex = 2;
            label9.Text = "Press the button to start listen test";
            // 
            // btnEnter
            // 
            btnEnter.Location = new Point(121, 59);
            btnEnter.Name = "btnEnter";
            btnEnter.Size = new Size(112, 34);
            btnEnter.TabIndex = 0;
            btnEnter.Text = "Start";
            btnEnter.UseVisualStyleBackColor = true;
            btnEnter.Click += btnEnter_Click;
            // 
            // groupBox6
            // 
            groupBox6.Controls.Add(label10);
            groupBox6.Controls.Add(label1);
            groupBox6.Location = new Point(6, 22);
            groupBox6.Name = "groupBox6";
            groupBox6.Size = new Size(311, 112);
            groupBox6.TabIndex = 6;
            groupBox6.TabStop = false;
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            label10.Location = new Point(12, 47);
            label10.Name = "label10";
            label10.Size = new Size(72, 32);
            label10.TabIndex = 3;
            label10.Text = "Time:";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new Font("Segoe UI", 14F, FontStyle.Regular, GraphicsUnit.Point);
            label5.Location = new Point(806, 57);
            label5.Name = "label5";
            label5.Size = new Size(0, 38);
            label5.TabIndex = 2;
            // 
            // groupBox2
            // 
            groupBox2.Controls.Add(btnSub);
            groupBox2.Location = new Point(724, 655);
            groupBox2.Name = "groupBox2";
            groupBox2.Size = new Size(747, 77);
            groupBox2.TabIndex = 3;
            groupBox2.TabStop = false;
            // 
            // groupBox3
            // 
            groupBox3.Controls.Add(pictureBox1);
            groupBox3.Controls.Add(txtQuestion);
            groupBox3.Location = new Point(12, 168);
            groupBox3.Name = "groupBox3";
            groupBox3.Size = new Size(706, 564);
            groupBox3.TabIndex = 4;
            groupBox3.TabStop = false;
            // 
            // pictureBox1
            // 
            pictureBox1.Location = new Point(6, 15);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.Size = new Size(694, 537);
            pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox1.TabIndex = 1;
            pictureBox1.TabStop = false;
            // 
            // txtQuestion
            // 
            txtQuestion.Font = new Font("Segoe UI", 16F, FontStyle.Regular, GraphicsUnit.Point);
            txtQuestion.Location = new Point(18, 30);
            txtQuestion.Multiline = true;
            txtQuestion.Name = "txtQuestion";
            txtQuestion.ReadOnly = true;
            txtQuestion.Size = new Size(682, 522);
            txtQuestion.TabIndex = 0;
            // 
            // groupBox4
            // 
            groupBox4.Location = new Point(724, 168);
            groupBox4.Name = "groupBox4";
            groupBox4.Size = new Size(236, 488);
            groupBox4.TabIndex = 5;
            groupBox4.TabStop = false;
            // 
            // groupBox5
            // 
            groupBox5.Controls.Add(button61);
            groupBox5.Controls.Add(button62);
            groupBox5.Controls.Add(button63);
            groupBox5.Controls.Add(button64);
            groupBox5.Controls.Add(button65);
            groupBox5.Controls.Add(button66);
            groupBox5.Controls.Add(button67);
            groupBox5.Controls.Add(button68);
            groupBox5.Controls.Add(button69);
            groupBox5.Controls.Add(button70);
            groupBox5.Controls.Add(button71);
            groupBox5.Controls.Add(button72);
            groupBox5.Controls.Add(button73);
            groupBox5.Controls.Add(button74);
            groupBox5.Controls.Add(button75);
            groupBox5.Controls.Add(button76);
            groupBox5.Controls.Add(button77);
            groupBox5.Controls.Add(button78);
            groupBox5.Controls.Add(button79);
            groupBox5.Controls.Add(button80);
            groupBox5.Controls.Add(button1);
            groupBox5.Controls.Add(button42);
            groupBox5.Controls.Add(button43);
            groupBox5.Controls.Add(button44);
            groupBox5.Controls.Add(button45);
            groupBox5.Controls.Add(button46);
            groupBox5.Controls.Add(button47);
            groupBox5.Controls.Add(button48);
            groupBox5.Controls.Add(button49);
            groupBox5.Controls.Add(button50);
            groupBox5.Controls.Add(button51);
            groupBox5.Controls.Add(button52);
            groupBox5.Controls.Add(button53);
            groupBox5.Controls.Add(button54);
            groupBox5.Controls.Add(button55);
            groupBox5.Controls.Add(button56);
            groupBox5.Controls.Add(button57);
            groupBox5.Controls.Add(button58);
            groupBox5.Controls.Add(button59);
            groupBox5.Controls.Add(button60);
            groupBox5.Controls.Add(button22);
            groupBox5.Controls.Add(button23);
            groupBox5.Controls.Add(button24);
            groupBox5.Controls.Add(button25);
            groupBox5.Controls.Add(button26);
            groupBox5.Controls.Add(button27);
            groupBox5.Controls.Add(button28);
            groupBox5.Controls.Add(button29);
            groupBox5.Controls.Add(button30);
            groupBox5.Controls.Add(button31);
            groupBox5.Controls.Add(button32);
            groupBox5.Controls.Add(button33);
            groupBox5.Controls.Add(button34);
            groupBox5.Controls.Add(button35);
            groupBox5.Controls.Add(button36);
            groupBox5.Controls.Add(button37);
            groupBox5.Controls.Add(button38);
            groupBox5.Controls.Add(button39);
            groupBox5.Controls.Add(button40);
            groupBox5.Controls.Add(button41);
            groupBox5.Controls.Add(button18);
            groupBox5.Controls.Add(button19);
            groupBox5.Controls.Add(button20);
            groupBox5.Controls.Add(button21);
            groupBox5.Controls.Add(button14);
            groupBox5.Controls.Add(button15);
            groupBox5.Controls.Add(button16);
            groupBox5.Controls.Add(button17);
            groupBox5.Controls.Add(button10);
            groupBox5.Controls.Add(button11);
            groupBox5.Controls.Add(button12);
            groupBox5.Controls.Add(button13);
            groupBox5.Controls.Add(button6);
            groupBox5.Controls.Add(button7);
            groupBox5.Controls.Add(button8);
            groupBox5.Controls.Add(button9);
            groupBox5.Controls.Add(button5);
            groupBox5.Controls.Add(button4);
            groupBox5.Controls.Add(button3);
            groupBox5.Controls.Add(button2);
            groupBox5.Controls.Add(label4);
            groupBox5.Controls.Add(label3);
            groupBox5.Location = new Point(966, 168);
            groupBox5.Name = "groupBox5";
            groupBox5.Size = new Size(505, 488);
            groupBox5.TabIndex = 6;
            groupBox5.TabStop = false;
            // 
            // button61
            // 
            button61.Location = new Point(449, 417);
            button61.Name = "button61";
            button61.Size = new Size(42, 34);
            button61.TabIndex = 81;
            button61.Text = "80";
            button61.UseVisualStyleBackColor = true;
            button61.Click += Button_Click;
            // 
            // button62
            // 
            button62.Location = new Point(449, 377);
            button62.Name = "button62";
            button62.Size = new Size(42, 34);
            button62.TabIndex = 80;
            button62.Text = "70";
            button62.UseVisualStyleBackColor = true;
            button62.Click += Button_Click;
            // 
            // button63
            // 
            button63.Location = new Point(449, 337);
            button63.Name = "button63";
            button63.Size = new Size(42, 34);
            button63.TabIndex = 79;
            button63.Text = "60";
            button63.UseVisualStyleBackColor = true;
            button63.Click += Button_Click;
            // 
            // button64
            // 
            button64.Location = new Point(449, 297);
            button64.Name = "button64";
            button64.Size = new Size(42, 34);
            button64.TabIndex = 78;
            button64.Text = "50";
            button64.UseVisualStyleBackColor = true;
            button64.Click += Button_Click;
            // 
            // button65
            // 
            button65.Location = new Point(401, 417);
            button65.Name = "button65";
            button65.Size = new Size(42, 34);
            button65.TabIndex = 77;
            button65.Text = "79";
            button65.UseVisualStyleBackColor = true;
            button65.Click += Button_Click;
            // 
            // button66
            // 
            button66.Location = new Point(401, 377);
            button66.Name = "button66";
            button66.Size = new Size(42, 34);
            button66.TabIndex = 76;
            button66.Text = "69";
            button66.UseVisualStyleBackColor = true;
            button66.Click += Button_Click;
            // 
            // button67
            // 
            button67.Location = new Point(401, 337);
            button67.Name = "button67";
            button67.Size = new Size(42, 34);
            button67.TabIndex = 75;
            button67.Text = "59";
            button67.UseVisualStyleBackColor = true;
            button67.Click += Button_Click;
            // 
            // button68
            // 
            button68.Location = new Point(401, 297);
            button68.Name = "button68";
            button68.Size = new Size(42, 34);
            button68.TabIndex = 74;
            button68.Text = "49";
            button68.UseVisualStyleBackColor = true;
            button68.Click += Button_Click;
            // 
            // button69
            // 
            button69.Location = new Point(353, 417);
            button69.Name = "button69";
            button69.Size = new Size(42, 34);
            button69.TabIndex = 73;
            button69.Text = "78";
            button69.UseVisualStyleBackColor = true;
            button69.Click += Button_Click;
            // 
            // button70
            // 
            button70.Location = new Point(353, 377);
            button70.Name = "button70";
            button70.Size = new Size(42, 34);
            button70.TabIndex = 72;
            button70.Text = "68";
            button70.UseVisualStyleBackColor = true;
            button70.Click += Button_Click;
            // 
            // button71
            // 
            button71.Location = new Point(353, 337);
            button71.Name = "button71";
            button71.Size = new Size(42, 34);
            button71.TabIndex = 71;
            button71.Text = "58";
            button71.UseVisualStyleBackColor = true;
            button71.Click += Button_Click;
            // 
            // button72
            // 
            button72.Location = new Point(353, 297);
            button72.Name = "button72";
            button72.Size = new Size(42, 34);
            button72.TabIndex = 70;
            button72.Text = "48";
            button72.UseVisualStyleBackColor = true;
            button72.Click += Button_Click;
            // 
            // button73
            // 
            button73.Location = new Point(305, 417);
            button73.Name = "button73";
            button73.Size = new Size(42, 34);
            button73.TabIndex = 69;
            button73.Text = "77";
            button73.UseVisualStyleBackColor = true;
            button73.Click += Button_Click;
            // 
            // button74
            // 
            button74.Location = new Point(305, 377);
            button74.Name = "button74";
            button74.Size = new Size(42, 34);
            button74.TabIndex = 68;
            button74.Text = "67";
            button74.UseVisualStyleBackColor = true;
            button74.Click += Button_Click;
            // 
            // button75
            // 
            button75.Location = new Point(305, 337);
            button75.Name = "button75";
            button75.Size = new Size(42, 34);
            button75.TabIndex = 67;
            button75.Text = "57";
            button75.UseVisualStyleBackColor = true;
            button75.Click += Button_Click;
            // 
            // button76
            // 
            button76.Location = new Point(305, 297);
            button76.Name = "button76";
            button76.Size = new Size(42, 34);
            button76.TabIndex = 66;
            button76.Text = "47";
            button76.UseVisualStyleBackColor = true;
            button76.Click += Button_Click;
            // 
            // button77
            // 
            button77.Location = new Point(257, 417);
            button77.Name = "button77";
            button77.Size = new Size(42, 34);
            button77.TabIndex = 65;
            button77.Text = "76";
            button77.UseVisualStyleBackColor = true;
            button77.Click += Button_Click;
            // 
            // button78
            // 
            button78.Location = new Point(257, 377);
            button78.Name = "button78";
            button78.Size = new Size(42, 34);
            button78.TabIndex = 64;
            button78.Text = "66";
            button78.UseVisualStyleBackColor = true;
            button78.Click += Button_Click;
            // 
            // button79
            // 
            button79.Location = new Point(257, 337);
            button79.Name = "button79";
            button79.Size = new Size(42, 34);
            button79.TabIndex = 63;
            button79.Text = "56";
            button79.UseVisualStyleBackColor = true;
            button79.Click += Button_Click;
            // 
            // button80
            // 
            button80.Location = new Point(257, 297);
            button80.Name = "button80";
            button80.Size = new Size(42, 34);
            button80.TabIndex = 62;
            button80.Text = "46";
            button80.UseVisualStyleBackColor = true;
            button80.Click += Button_Click;
            // 
            // button1
            // 
            button1.Location = new Point(449, 198);
            button1.Name = "button1";
            button1.Size = new Size(42, 34);
            button1.TabIndex = 61;
            button1.Text = "40";
            button1.UseVisualStyleBackColor = true;
            button1.Click += Button_Click;
            // 
            // button42
            // 
            button42.Location = new Point(209, 198);
            button42.Name = "button42";
            button42.Size = new Size(42, 34);
            button42.TabIndex = 60;
            button42.Text = "35";
            button42.UseVisualStyleBackColor = true;
            button42.Click += Button_Click;
            // 
            // button43
            // 
            button43.Location = new Point(449, 158);
            button43.Name = "button43";
            button43.Size = new Size(42, 34);
            button43.TabIndex = 59;
            button43.Text = "30";
            button43.UseVisualStyleBackColor = true;
            button43.Click += Button_Click;
            // 
            // button44
            // 
            button44.Location = new Point(209, 158);
            button44.Name = "button44";
            button44.Size = new Size(42, 34);
            button44.TabIndex = 58;
            button44.Text = "25";
            button44.UseVisualStyleBackColor = true;
            button44.Click += Button_Click;
            // 
            // button45
            // 
            button45.Location = new Point(401, 198);
            button45.Name = "button45";
            button45.Size = new Size(42, 34);
            button45.TabIndex = 57;
            button45.Text = "39";
            button45.UseVisualStyleBackColor = true;
            button45.Click += Button_Click;
            // 
            // button46
            // 
            button46.Location = new Point(161, 198);
            button46.Name = "button46";
            button46.Size = new Size(42, 34);
            button46.TabIndex = 56;
            button46.Text = "34";
            button46.UseVisualStyleBackColor = true;
            button46.Click += Button_Click;
            // 
            // button47
            // 
            button47.Location = new Point(401, 158);
            button47.Name = "button47";
            button47.Size = new Size(42, 34);
            button47.TabIndex = 55;
            button47.Text = "29";
            button47.UseVisualStyleBackColor = true;
            button47.Click += Button_Click;
            // 
            // button48
            // 
            button48.Location = new Point(161, 158);
            button48.Name = "button48";
            button48.Size = new Size(42, 34);
            button48.TabIndex = 54;
            button48.Text = "24";
            button48.UseVisualStyleBackColor = true;
            button48.Click += Button_Click;
            // 
            // button49
            // 
            button49.Location = new Point(353, 198);
            button49.Name = "button49";
            button49.Size = new Size(42, 34);
            button49.TabIndex = 53;
            button49.Text = "38";
            button49.UseVisualStyleBackColor = true;
            button49.Click += Button_Click;
            // 
            // button50
            // 
            button50.Location = new Point(113, 198);
            button50.Name = "button50";
            button50.Size = new Size(42, 34);
            button50.TabIndex = 52;
            button50.Text = "33";
            button50.UseVisualStyleBackColor = true;
            button50.Click += Button_Click;
            // 
            // button51
            // 
            button51.Location = new Point(353, 158);
            button51.Name = "button51";
            button51.Size = new Size(42, 34);
            button51.TabIndex = 51;
            button51.Text = "28";
            button51.UseVisualStyleBackColor = true;
            button51.Click += Button_Click;
            // 
            // button52
            // 
            button52.Location = new Point(113, 158);
            button52.Name = "button52";
            button52.Size = new Size(42, 34);
            button52.TabIndex = 50;
            button52.Text = "23";
            button52.UseVisualStyleBackColor = true;
            button52.Click += Button_Click;
            // 
            // button53
            // 
            button53.Location = new Point(305, 198);
            button53.Name = "button53";
            button53.Size = new Size(42, 34);
            button53.TabIndex = 49;
            button53.Text = "37";
            button53.UseVisualStyleBackColor = true;
            button53.Click += Button_Click;
            // 
            // button54
            // 
            button54.Location = new Point(65, 198);
            button54.Name = "button54";
            button54.Size = new Size(42, 34);
            button54.TabIndex = 48;
            button54.Text = "32";
            button54.UseVisualStyleBackColor = true;
            button54.Click += Button_Click;
            // 
            // button55
            // 
            button55.Location = new Point(305, 158);
            button55.Name = "button55";
            button55.Size = new Size(42, 34);
            button55.TabIndex = 47;
            button55.Text = "27";
            button55.UseVisualStyleBackColor = true;
            button55.Click += Button_Click;
            // 
            // button56
            // 
            button56.Location = new Point(65, 158);
            button56.Name = "button56";
            button56.Size = new Size(42, 34);
            button56.TabIndex = 46;
            button56.Text = "22";
            button56.UseVisualStyleBackColor = true;
            button56.Click += Button_Click;
            // 
            // button57
            // 
            button57.Location = new Point(257, 198);
            button57.Name = "button57";
            button57.Size = new Size(42, 34);
            button57.TabIndex = 45;
            button57.Text = "36";
            button57.UseVisualStyleBackColor = true;
            button57.Click += Button_Click;
            // 
            // button58
            // 
            button58.Location = new Point(17, 198);
            button58.Name = "button58";
            button58.Size = new Size(42, 34);
            button58.TabIndex = 44;
            button58.Text = "31";
            button58.UseVisualStyleBackColor = true;
            button58.Click += Button_Click;
            // 
            // button59
            // 
            button59.Location = new Point(257, 158);
            button59.Name = "button59";
            button59.Size = new Size(42, 34);
            button59.TabIndex = 43;
            button59.Text = "26";
            button59.UseVisualStyleBackColor = true;
            button59.Click += Button_Click;
            // 
            // button60
            // 
            button60.Location = new Point(17, 158);
            button60.Name = "button60";
            button60.Size = new Size(42, 34);
            button60.TabIndex = 42;
            button60.Text = "21";
            button60.UseVisualStyleBackColor = true;
            button60.Click += Button_Click;
            // 
            // button22
            // 
            button22.Location = new Point(209, 417);
            button22.Name = "button22";
            button22.Size = new Size(42, 34);
            button22.TabIndex = 41;
            button22.Text = "75";
            button22.UseVisualStyleBackColor = true;
            button22.Click += Button_Click;
            // 
            // button23
            // 
            button23.Location = new Point(209, 377);
            button23.Name = "button23";
            button23.Size = new Size(42, 34);
            button23.TabIndex = 40;
            button23.Text = "65";
            button23.UseVisualStyleBackColor = true;
            button23.Click += Button_Click;
            // 
            // button24
            // 
            button24.Location = new Point(209, 337);
            button24.Name = "button24";
            button24.Size = new Size(42, 34);
            button24.TabIndex = 39;
            button24.Text = "55";
            button24.UseVisualStyleBackColor = true;
            button24.Click += Button_Click;
            // 
            // button25
            // 
            button25.Location = new Point(209, 297);
            button25.Name = "button25";
            button25.Size = new Size(42, 34);
            button25.TabIndex = 38;
            button25.Text = "45";
            button25.UseVisualStyleBackColor = true;
            button25.Click += Button_Click;
            // 
            // button26
            // 
            button26.Location = new Point(161, 417);
            button26.Name = "button26";
            button26.Size = new Size(42, 34);
            button26.TabIndex = 37;
            button26.Text = "74";
            button26.UseVisualStyleBackColor = true;
            button26.Click += Button_Click;
            // 
            // button27
            // 
            button27.Location = new Point(161, 377);
            button27.Name = "button27";
            button27.Size = new Size(42, 34);
            button27.TabIndex = 36;
            button27.Text = "64";
            button27.UseVisualStyleBackColor = true;
            button27.Click += Button_Click;
            // 
            // button28
            // 
            button28.Location = new Point(161, 337);
            button28.Name = "button28";
            button28.Size = new Size(42, 34);
            button28.TabIndex = 35;
            button28.Text = "54";
            button28.UseVisualStyleBackColor = true;
            button28.Click += Button_Click;
            // 
            // button29
            // 
            button29.Location = new Point(161, 297);
            button29.Name = "button29";
            button29.Size = new Size(42, 34);
            button29.TabIndex = 34;
            button29.Text = "44";
            button29.UseVisualStyleBackColor = true;
            button29.Click += Button_Click;
            // 
            // button30
            // 
            button30.Location = new Point(113, 417);
            button30.Name = "button30";
            button30.Size = new Size(42, 34);
            button30.TabIndex = 33;
            button30.Text = "73";
            button30.UseVisualStyleBackColor = true;
            button30.Click += Button_Click;
            // 
            // button31
            // 
            button31.Location = new Point(113, 377);
            button31.Name = "button31";
            button31.Size = new Size(42, 34);
            button31.TabIndex = 32;
            button31.Text = "63";
            button31.UseVisualStyleBackColor = true;
            button31.Click += Button_Click;
            // 
            // button32
            // 
            button32.Location = new Point(113, 337);
            button32.Name = "button32";
            button32.Size = new Size(42, 34);
            button32.TabIndex = 31;
            button32.Text = "53";
            button32.UseVisualStyleBackColor = true;
            button32.Click += Button_Click;
            // 
            // button33
            // 
            button33.Location = new Point(113, 297);
            button33.Name = "button33";
            button33.Size = new Size(42, 34);
            button33.TabIndex = 30;
            button33.Text = "43";
            button33.UseVisualStyleBackColor = true;
            button33.Click += Button_Click;
            // 
            // button34
            // 
            button34.Location = new Point(65, 417);
            button34.Name = "button34";
            button34.Size = new Size(42, 34);
            button34.TabIndex = 29;
            button34.Text = "72";
            button34.UseVisualStyleBackColor = true;
            button34.Click += Button_Click;
            // 
            // button35
            // 
            button35.Location = new Point(65, 377);
            button35.Name = "button35";
            button35.Size = new Size(42, 34);
            button35.TabIndex = 28;
            button35.Text = "62";
            button35.UseVisualStyleBackColor = true;
            button35.Click += Button_Click;
            // 
            // button36
            // 
            button36.Location = new Point(65, 337);
            button36.Name = "button36";
            button36.Size = new Size(42, 34);
            button36.TabIndex = 27;
            button36.Text = "52";
            button36.UseVisualStyleBackColor = true;
            button36.Click += Button_Click;
            // 
            // button37
            // 
            button37.Location = new Point(65, 297);
            button37.Name = "button37";
            button37.Size = new Size(42, 34);
            button37.TabIndex = 26;
            button37.Text = "42";
            button37.UseVisualStyleBackColor = true;
            button37.Click += Button_Click;
            // 
            // button38
            // 
            button38.Location = new Point(17, 417);
            button38.Name = "button38";
            button38.Size = new Size(42, 34);
            button38.TabIndex = 25;
            button38.Text = "71";
            button38.UseVisualStyleBackColor = true;
            button38.Click += Button_Click;
            // 
            // button39
            // 
            button39.Location = new Point(17, 377);
            button39.Name = "button39";
            button39.Size = new Size(42, 34);
            button39.TabIndex = 24;
            button39.Text = "61";
            button39.UseVisualStyleBackColor = true;
            button39.Click += Button_Click;
            // 
            // button40
            // 
            button40.Location = new Point(17, 337);
            button40.Name = "button40";
            button40.Size = new Size(42, 34);
            button40.TabIndex = 23;
            button40.Text = "51";
            button40.UseVisualStyleBackColor = true;
            button40.Click += Button_Click;
            // 
            // button41
            // 
            button41.Location = new Point(17, 297);
            button41.Name = "button41";
            button41.Size = new Size(42, 34);
            button41.TabIndex = 22;
            button41.Text = "41";
            button41.UseVisualStyleBackColor = true;
            button41.Click += Button_Click;
            // 
            // button18
            // 
            button18.Location = new Point(449, 118);
            button18.Name = "button18";
            button18.Size = new Size(42, 34);
            button18.TabIndex = 21;
            button18.Text = "20";
            button18.UseVisualStyleBackColor = true;
            button18.Click += Button_Click;
            // 
            // button19
            // 
            button19.Location = new Point(209, 118);
            button19.Name = "button19";
            button19.Size = new Size(42, 34);
            button19.TabIndex = 20;
            button19.Text = "15";
            button19.UseVisualStyleBackColor = true;
            button19.Click += Button_Click;
            // 
            // button20
            // 
            button20.Location = new Point(449, 78);
            button20.Name = "button20";
            button20.Size = new Size(42, 34);
            button20.TabIndex = 19;
            button20.Text = "10";
            button20.UseVisualStyleBackColor = true;
            button20.Click += Button_Click;
            // 
            // button21
            // 
            button21.Location = new Point(209, 78);
            button21.Name = "button21";
            button21.Size = new Size(42, 34);
            button21.TabIndex = 18;
            button21.Text = "5";
            button21.UseVisualStyleBackColor = true;
            button21.Click += Button_Click;
            // 
            // button14
            // 
            button14.Location = new Point(401, 118);
            button14.Name = "button14";
            button14.Size = new Size(42, 34);
            button14.TabIndex = 17;
            button14.Text = "19";
            button14.UseVisualStyleBackColor = true;
            button14.Click += Button_Click;
            // 
            // button15
            // 
            button15.Location = new Point(161, 118);
            button15.Name = "button15";
            button15.Size = new Size(42, 34);
            button15.TabIndex = 16;
            button15.Text = "14";
            button15.UseVisualStyleBackColor = true;
            button15.Click += Button_Click;
            // 
            // button16
            // 
            button16.Location = new Point(401, 78);
            button16.Name = "button16";
            button16.Size = new Size(42, 34);
            button16.TabIndex = 15;
            button16.Text = "9";
            button16.UseVisualStyleBackColor = true;
            button16.Click += Button_Click;
            // 
            // button17
            // 
            button17.Location = new Point(161, 78);
            button17.Name = "button17";
            button17.Size = new Size(42, 34);
            button17.TabIndex = 14;
            button17.Text = "4";
            button17.UseVisualStyleBackColor = true;
            button17.Click += Button_Click;
            // 
            // button10
            // 
            button10.Location = new Point(353, 118);
            button10.Name = "button10";
            button10.Size = new Size(42, 34);
            button10.TabIndex = 13;
            button10.Text = "18";
            button10.UseVisualStyleBackColor = true;
            button10.Click += Button_Click;
            // 
            // button11
            // 
            button11.Location = new Point(113, 118);
            button11.Name = "button11";
            button11.Size = new Size(42, 34);
            button11.TabIndex = 12;
            button11.Text = "13";
            button11.UseVisualStyleBackColor = true;
            button11.Click += Button_Click;
            // 
            // button12
            // 
            button12.Location = new Point(353, 78);
            button12.Name = "button12";
            button12.Size = new Size(42, 34);
            button12.TabIndex = 11;
            button12.Text = "8";
            button12.UseVisualStyleBackColor = true;
            button12.Click += Button_Click;
            // 
            // button13
            // 
            button13.Location = new Point(113, 78);
            button13.Name = "button13";
            button13.Size = new Size(42, 34);
            button13.TabIndex = 10;
            button13.Text = "3";
            button13.UseVisualStyleBackColor = true;
            button13.Click += Button_Click;
            // 
            // button6
            // 
            button6.Location = new Point(305, 118);
            button6.Name = "button6";
            button6.Size = new Size(42, 34);
            button6.TabIndex = 9;
            button6.Text = "17";
            button6.UseVisualStyleBackColor = true;
            button6.Click += Button_Click;
            // 
            // button7
            // 
            button7.Location = new Point(65, 118);
            button7.Name = "button7";
            button7.Size = new Size(42, 34);
            button7.TabIndex = 8;
            button7.Text = "12";
            button7.UseVisualStyleBackColor = true;
            button7.Click += Button_Click;
            // 
            // button8
            // 
            button8.Location = new Point(305, 78);
            button8.Name = "button8";
            button8.Size = new Size(42, 34);
            button8.TabIndex = 7;
            button8.Text = "7";
            button8.UseVisualStyleBackColor = true;
            button8.Click += Button_Click;
            // 
            // button9
            // 
            button9.Location = new Point(65, 78);
            button9.Name = "button9";
            button9.Size = new Size(42, 34);
            button9.TabIndex = 6;
            button9.Text = "2";
            button9.UseVisualStyleBackColor = true;
            button9.Click += Button_Click;
            // 
            // button5
            // 
            button5.Location = new Point(257, 118);
            button5.Name = "button5";
            button5.Size = new Size(42, 34);
            button5.TabIndex = 5;
            button5.Text = "16";
            button5.UseVisualStyleBackColor = true;
            button5.Click += Button_Click;
            // 
            // button4
            // 
            button4.Location = new Point(17, 118);
            button4.Name = "button4";
            button4.Size = new Size(42, 34);
            button4.TabIndex = 4;
            button4.Text = "11";
            button4.UseVisualStyleBackColor = true;
            button4.Click += Button_Click;
            // 
            // button3
            // 
            button3.Location = new Point(257, 78);
            button3.Name = "button3";
            button3.Size = new Size(42, 34);
            button3.TabIndex = 3;
            button3.Text = "6";
            button3.UseVisualStyleBackColor = true;
            button3.Click += Button_Click;
            // 
            // button2
            // 
            button2.Location = new Point(17, 78);
            button2.Name = "button2";
            button2.Size = new Size(42, 34);
            button2.TabIndex = 2;
            button2.Text = "1";
            button2.UseVisualStyleBackColor = true;
            button2.Click += Button_Click;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new Font("Segoe UI", 11F, FontStyle.Bold, GraphicsUnit.Point);
            label4.Location = new Point(6, 246);
            label4.Name = "label4";
            label4.Size = new Size(61, 30);
            label4.TabIndex = 1;
            label4.Text = "阅读";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new Font("Segoe UI", 11F, FontStyle.Bold, GraphicsUnit.Point);
            label3.Location = new Point(6, 30);
            label3.Name = "label3";
            label3.Size = new Size(61, 30);
            label3.TabIndex = 0;
            label3.Text = "听力";
            // 
            // Form3_3_3_3
            // 
            AutoScaleDimensions = new SizeF(10F, 25F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1475, 751);
            Controls.Add(groupBox5);
            Controls.Add(groupBox4);
            Controls.Add(groupBox3);
            Controls.Add(groupBox2);
            Controls.Add(groupBox1);
            Name = "Form3_3_3_3";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Form3_3_3_3";
            Load += Form3_3_3_3_Load;
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            groupBox8.ResumeLayout(false);
            groupBox8.PerformLayout();
            groupBox7.ResumeLayout(false);
            groupBox7.PerformLayout();
            groupBox6.ResumeLayout(false);
            groupBox6.PerformLayout();
            groupBox2.ResumeLayout(false);
            groupBox3.ResumeLayout(false);
            groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).EndInit();
            groupBox5.ResumeLayout(false);
            groupBox5.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private Label label1;
        private Button btnSub;
        private GroupBox groupBox1;
        private Label label2;
        private GroupBox groupBox2;
        private GroupBox groupBox3;
        private GroupBox groupBox4;
        private GroupBox groupBox5;
        private Button button2;
        private Label label4;
        private Label label3;
        private TextBox txtQuestion;
        private PictureBox pictureBox1;
        private Button button22;
        private Button button23;
        private Button button24;
        private Button button25;
        private Button button26;
        private Button button27;
        private Button button28;
        private Button button29;
        private Button button30;
        private Button button31;
        private Button button32;
        private Button button33;
        private Button button34;
        private Button button35;
        private Button button36;
        private Button button37;
        private Button button38;
        private Button button39;
        private Button button40;
        private Button button41;
        private Button button18;
        private Button button19;
        private Button button20;
        private Button button21;
        private Button button14;
        private Button button15;
        private Button button16;
        private Button button17;
        private Button button10;
        private Button button11;
        private Button button12;
        private Button button13;
        private Button button6;
        private Button button7;
        private Button button8;
        private Button button9;
        private Button button5;
        private Button button4;
        private Button button3;
        private Label label5;
        private Label label6;
        private Label label8;
        private Label label7;
        private GroupBox groupBox6;
        private GroupBox groupBox8;
        private GroupBox groupBox7;
        private Label label9;
        private Button btnEnter;
        private Label label10;
        private Button button61;
        private Button button62;
        private Button button63;
        private Button button64;
        private Button button65;
        private Button button66;
        private Button button67;
        private Button button68;
        private Button button69;
        private Button button70;
        private Button button71;
        private Button button72;
        private Button button73;
        private Button button74;
        private Button button75;
        private Button button76;
        private Button button77;
        private Button button78;
        private Button button79;
        private Button button80;
        private Button button1;
        private Button button42;
        private Button button43;
        private Button button44;
        private Button button45;
        private Button button46;
        private Button button47;
        private Button button48;
        private Button button49;
        private Button button50;
        private Button button51;
        private Button button52;
        private Button button53;
        private Button button54;
        private Button button55;
        private Button button56;
        private Button button57;
        private Button button58;
        private Button button59;
        private Button button60;
    }
}