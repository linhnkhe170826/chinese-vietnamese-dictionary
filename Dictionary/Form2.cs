﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dictionary
{
    public partial class FrmPractice : Form
    {
        public FrmPractice()
        {
            InitializeComponent();
        }
        Practice practice = new Practice();
        static String GetConnectionString()
        {
            IConfiguration config = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", true, true).Build();
            var strConnection = config.GetConnectionString("MyDB");
            return strConnection;
        }
        private void LoadDataGridView()
        {
            List<Practice> list = new List<Practice>();
            list = practice.GetPractices();
            list = list.Where(x => x.word.Contains(txtSearch.Text)).ToList();
            if (!cbDifficult.Text.Equals("All"))
            {
                if (cbAlphabet.Text.Equals("Từ A-Z"))
                {
                    list = list.Where(x => x.word.Contains(txtSearch.Text) && x.dificult == Int32.Parse(cbDifficult.Text)).OrderBy(x => x.word).ToList();
                }
                else list = list.Where(x => x.word.Contains(txtSearch.Text) && x.dificult == Int32.Parse(cbDifficult.Text)).OrderByDescending(x => x.word).ToList();
            } //list = list.Where(x => x.dificult == Int32.Parse(cbDifficult.Text)).ToList();

            dataGridView1.DataSource = list;
        }
        private void FrmPractice_Load(object sender, EventArgs e)
        {
            List<string> dif = practice.GetDificult();
            dif.Insert(0, "All");
            cbDifficult.DataSource = dif;
            cbDifficult.SelectedIndex = 0;
            List<string> alphabetSortingOptions = new List<string> { "Từ A-Z", "Từ Z-A" };
            cbAlphabet.DataSource = alphabetSortingOptions;
            LoadDataGridView();
        }

        private void lbTest_Click(object sender, EventArgs e)
        {
            FrmTest frmTest = new FrmTest();
            frmTest.FormClosed += FrmTest_FormClosed;
            frmTest.Show();
            this.Hide();
        }
        private void FrmTest_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close(); // Đóng form FrmSearchWord khi form FrmTest được đóng
        }

        private void lbPractice_Click(object sender, EventArgs e)
        {
            FrmPractice_Load(sender, e);
        }

        private void lbSearchWord_Click(object sender, EventArgs e)
        {
            FrmSearchWord frmSearchWord = new FrmSearchWord();
            frmSearchWord.FormClosed += FrmSearchWord_FormClosed;
            frmSearchWord.Show();
            this.Hide();
        }
        private void FrmSearchWord_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close(); // Đóng form FrmSearchWord khi form FrmTest được đóng
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataGridViewRow selectedRow = dataGridView1.SelectedRows[0];
            txtWord.Text = selectedRow.Cells[0].Value.ToString();
            txtMean.Text = selectedRow.Cells[1].Value.ToString();

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Bạn có muốn xóa từ này ra khỏi danh sách các từ luyện tập?", "Caution", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes) practice.DeleteWord(txtWord.Text);
            txtMean.Text = string.Empty;
            txtWord.Text = string.Empty;
            LoadDataGridView();
        }

        private void cbAlphabet_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*List<Practice> list = practice.GetPractices();
            if (cbAlphabet.Text.Equals("Từ A-Z"))
            {
                list = list.OrderBy(x => x.word).ToList();
            }
            else list = list.OrderByDescending(x => x.word).ToList();
            dataGridView1.DataSource = list;*/
            LoadDataGridView();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            //List<Practice> list = practice.GetPractices();
            //list = list.Where(x => x.word.Contains(txtSearch.Text)).ToList();
            //dataGridView1.DataSource = list;
            LoadDataGridView();
        }

        private void cbDifficult_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataGridView();
            //List<Practice> list = practice.GetPractices();
            //if(!cbDifficult.Text.Equals("All")) list = list.Where(x => x.dificult == Convert.ToUInt32(cbDifficult.Text)).ToList();
            //dataGridView1.DataSource = list;
        }

        private void btnPractice_Click(object sender, EventArgs e)
        {
            Form2_2 form2_2 = new Form2_2();
            form2_2.FormClosed += Form2_2_FormClosed;
            form2_2.Show();
            this.Hide();
        }
        private void Form2_2_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close(); // Đóng form FrmSearchWord khi form FrmTest được đóng
        }
    }
}
