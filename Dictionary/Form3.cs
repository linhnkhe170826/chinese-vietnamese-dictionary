﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dictionary
{
    public partial class FrmTest : Form
    {
        public FrmTest()
        {
            InitializeComponent();
        }

        private void frmTest_Load(object sender, EventArgs e)
        {

        }

        private void lbSearchWord_Click(object sender, EventArgs e)
        {
            FrmSearchWord frmSearchWord = new FrmSearchWord();
            frmSearchWord.FormClosed += FrmSearchWord_FormClosed;
            frmSearchWord.Show();
            this.Hide();
        }
        private void FrmSearchWord_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close(); // Đóng form FrmSearchWord khi form FrmTest được đóng
        }

        private void lbPractice_Click(object sender, EventArgs e)
        {
            FrmPractice frmPractice = new FrmPractice();
            frmPractice.FormClosed += FrmPractice_FormClosed; //Đóng cái frmPractice lại nè
            frmPractice.Show(); //Hiển thị from Practice
            this.Hide(); // Ẩn form FrmSearchWord khi mở form FrmPractice
        }

        private void FrmPractice_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close(); // Đóng form FrmSearchWord khi form FrmPractice được đóng
        }

        private void lbTest_Click(object sender, EventArgs e)
        {
            frmTest_Load(sender, e);
        }

        private void btnHSK1_Click(object sender, EventArgs e)
        {
            btnClick("HSK1");
        }
        private void btnClick(string type)
        {
            Form3_3 form3_3 = new Form3_3(type);
            form3_3.FormClosed += Form3_3_FormClosed; //Đóng cái frmPractice lại nè
            form3_3.Show(); //Hiển thị from Practice
            this.Hide(); // Ẩn form FrmSearchWord khi mở form FrmPractice
        }

        private void Form3_3_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close(); // Đóng form FrmSearchWord khi form FrmPractice được đóng
        }

        private void btnHSK2_Click(object sender, EventArgs e)
        {
            btnClick("HSK2");
        }

        private void btnHSK3_Click(object sender, EventArgs e)
        {
            btnClick("HSK3");
        }

        private void btnHSK4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This part is currently still under development","Cation",MessageBoxButtons.OK,MessageBoxIcon.Warning);
        }

        private void btnHSK5_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This part is currently still under development", "Cation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void btnHSK6_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This part is currently still under development", "Cation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
