﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.DataFormats;

namespace Dictionary
{
    public partial class Form1_1 : Form
    {
        string rootDirectory = Directory.GetParent(Directory.GetParent(Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.FullName).Parent.FullName).Parent.FullName;
        string dataW;
        string dataM;
        int type;
        public Form1_1()
        {
            InitializeComponent();
        }

        public Form1_1(string word, string mean, int t)
        {
            dataW = word;
            dataM = mean;
            type = t;
            InitializeComponent();
        }

        private void Form1_1_Load(object sender, EventArgs e)
        {
            txtWord.Text = dataW;
            txtMean.Text = dataM;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string filePath;
            if (type == 0)
            {
                filePath = rootDirectory + @"\Dictionary\Pinyin.txt";
                //filePath = @"\Dictionary\TestGhi1.txt";
            }
            else filePath = rootDirectory + @"\Dictionary\Han_Viet.txt";

            if (txtMean.Text.Equals(dataM))
            {
                this.Close();
            }
            else
            {
                if (dataM != "")
                {
                    GhiDeDuLieu(txtMean.Text,filePath);
                }
                else
                {
                    GhiDuLieuMoi(txtMean.Text,filePath);
                }
                this.Close();
            }
        }

        private void GhiDeDuLieu(string data, string filePath)
        {
            //string filePath = rootDirectory + @"\Dictionary\Pinyin.txt";
            //filePath = rootDirectory + @"\Dictionary\TestGhi.txt";
            string newData = data;
            try
            {
                // Đọc toàn bộ nội dung của file vào bộ nhớ
                string[] lines = File.ReadAllLines(filePath);

                // Ghi đè dữ liệu mới
                using (StreamWriter sw = new StreamWriter(filePath))
                {
                    // Ghi đè dữ liệu mới vào file
                    foreach (string line in lines)
                    {
                        if (line.StartsWith(dataW))
                        {
                            if (type == 0)
                            {
                                sw.WriteLine(FixBanGhi(newData)); // Ghi đè đoạn dữ liệu mới
                            }
                            else sw.WriteLine(FixBanGhiHV(newData)); // Ghi đè đoạn dữ liệu mới

                        }
                        else
                        {
                            sw.WriteLine(line); // Ghi lại dòng gốc nếu không phải dòng cần ghi đè
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private void GhiDuLieuMoi(string data,string filePath)
        {
            string newData = data;
            try
            {
                    // Mở file và ghi dữ liệu vào cuối file
                    using (StreamWriter writer = File.AppendText(filePath))
                    {
                        if(type == 0)
                        {
                            writer.WriteLine(FixBanGhi(newData));
                        }
                        else writer.WriteLine(FixBanGhiHV(newData));
                    }
             }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private string FixBanGhi(string data)
        {
            data = dataW + "=" + data.Replace(Environment.NewLine, "\\n\\t").Replace(Environment.NewLine + '✚', "\\n✚");
            return data;
        }

        private string FixBanGhiHV(string data)
        {
            data = Environment.NewLine + data.Replace(Environment.NewLine, "\\n\\t").Replace(Environment.NewLine + '✚', "\\n✚") + "=" + dataW;
            return data;
        }

    }
}
