﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dictionary
{
    internal class StudentRespone
    {
        public int ResponsiveId { get; set; }
        public int ResultId { get; set; }
        public int QuestionId { get; set; }
        public string SelectedAnswer { get; set; }

        public StudentRespone(int responsiveId, int resultId, int questionId, string selectedAnswer)
        {
            ResponsiveId = responsiveId;
            ResultId = resultId;
            QuestionId = questionId;
            SelectedAnswer = selectedAnswer;
        }

        public StudentRespone(int resultId, int questionId, string selectedAnswer)
        {
            ResultId = resultId;
            QuestionId = questionId;
            SelectedAnswer = selectedAnswer;
        }

        public StudentRespone() { }

        SqlConnection connection;
        SqlCommand command;
        static String GetConnectionString()
        {
            IConfiguration config = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", true, true).Build();
            var strConnection = config.GetConnectionString("MyDB");
            return strConnection;
        }
        public int AddResult(int examid)
        {
            int resultId = 0;
            connection = new SqlConnection(GetConnectionString());
            string Query = "  Insert into Results values (@ExamId,0,@TodayDate) SELECT @@IDENTITY AS NewID;";
            command = new SqlCommand(Query, connection);
            command.Parameters.AddWithValue("@ExamId", examid); 
            command.Parameters.AddWithValue("@TodayDate", DateTime.Now);
            try
            {
                connection.Open();
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        resultId = Int32.Parse(reader.GetDecimal("NewID").ToString());
                    }
                    
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally { connection.Close(); }

            return resultId;
        }
        public void UpdateResult(int Score, int resultId)
        {
            connection = new SqlConnection(GetConnectionString());
            string Query = "  Update Results set Score = @Score where resultId = @resultId";
            command = new SqlCommand(Query, connection);
            command.Parameters.AddWithValue("@Score", Score);
            command.Parameters.AddWithValue("@resultId", resultId);
            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally { connection.Close(); }
        }
        public void AddResspone(StudentRespone respone)
        {
            connection = new SqlConnection(GetConnectionString());
            string Query = "Insert into StudentResponses values (@ResultId,@QuestionId,@SelectedAnswer);";
            command = new SqlCommand(Query, connection);
            command.Parameters.AddWithValue("@ResultId", respone.ResultId);
            command.Parameters.AddWithValue("@QuestionId", respone.QuestionId); 
            command.Parameters.AddWithValue("@SelectedAnswer", respone.SelectedAnswer);
            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally { connection.Close(); }
        }
        public void UpdateResspone(StudentRespone respone)
        {
            connection = new SqlConnection(GetConnectionString());
            string Query = "Update StudentResponses set SelectedAnswer = @SelectedAnswer Where ResponseID = @ResponseID;";
            command = new SqlCommand(Query, connection);
            command.Parameters.AddWithValue("@ResponseID", respone.ResponsiveId);
            command.Parameters.AddWithValue("@SelectedAnswer", respone.SelectedAnswer);
            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally { connection.Close(); }
        }
        public StudentRespone FindResspone(int questionId, int ResultID)
        {
            StudentRespone respone = new StudentRespone();
            connection = new SqlConnection(GetConnectionString());
            string Query = " select * from StudentResponses where ResultID = @ResultId and QuestionID = @QuestionId";
            command = new SqlCommand(Query, connection);
            command.Parameters.AddWithValue("@ResultId", ResultID);
            command.Parameters.AddWithValue("@QuestionId", questionId);
            try
            {
                connection.Open();
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        respone.ResponsiveId = reader.GetInt32("ResponseID");
                        respone.QuestionId = reader.GetInt32("ResultID");
                        respone.ResultId = reader.GetInt32("QuestionID");
                        respone.SelectedAnswer = reader.GetString("SelectedAnswer");
                    }

                }else respone = null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally { connection.Close(); }
            return respone;
        }
    }
}
