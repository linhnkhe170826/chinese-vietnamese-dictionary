﻿namespace Dictionary
{
    partial class Form3_3_3_1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            timer1 = new System.Windows.Forms.Timer(components);
            label1 = new Label();
            btnSub = new Button();
            groupBox1 = new GroupBox();
            groupBox8 = new GroupBox();
            label2 = new Label();
            label6 = new Label();
            label7 = new Label();
            label8 = new Label();
            groupBox7 = new GroupBox();
            label9 = new Label();
            btnEnter = new Button();
            groupBox6 = new GroupBox();
            label10 = new Label();
            label5 = new Label();
            groupBox2 = new GroupBox();
            groupBox3 = new GroupBox();
            pictureBox1 = new PictureBox();
            txtQuestion = new TextBox();
            groupBox4 = new GroupBox();
            groupBox5 = new GroupBox();
            button22 = new Button();
            button23 = new Button();
            button24 = new Button();
            button25 = new Button();
            button26 = new Button();
            button27 = new Button();
            button28 = new Button();
            button29 = new Button();
            button30 = new Button();
            button31 = new Button();
            button32 = new Button();
            button33 = new Button();
            button34 = new Button();
            button35 = new Button();
            button36 = new Button();
            button37 = new Button();
            button38 = new Button();
            button39 = new Button();
            button40 = new Button();
            button41 = new Button();
            button18 = new Button();
            button19 = new Button();
            button20 = new Button();
            button21 = new Button();
            button14 = new Button();
            button15 = new Button();
            button16 = new Button();
            button17 = new Button();
            button10 = new Button();
            button11 = new Button();
            button12 = new Button();
            button13 = new Button();
            button6 = new Button();
            button7 = new Button();
            button8 = new Button();
            button9 = new Button();
            button5 = new Button();
            button4 = new Button();
            button3 = new Button();
            button2 = new Button();
            label4 = new Label();
            label3 = new Label();
            groupBox1.SuspendLayout();
            groupBox8.SuspendLayout();
            groupBox7.SuspendLayout();
            groupBox6.SuspendLayout();
            groupBox2.SuspendLayout();
            groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).BeginInit();
            groupBox5.SuspendLayout();
            SuspendLayout();
            // 
            // timer1
            // 
            timer1.Interval = 10000;
            timer1.Tick += timer1_Tick;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 24F, FontStyle.Regular, GraphicsUnit.Point);
            label1.Location = new Point(115, 27);
            label1.Name = "label1";
            label1.Size = new Size(155, 65);
            label1.TabIndex = 0;
            label1.Text = "label1";
            // 
            // btnSub
            // 
            btnSub.Enabled = false;
            btnSub.Location = new Point(380, 30);
            btnSub.Name = "btnSub";
            btnSub.Size = new Size(117, 35);
            btnSub.TabIndex = 1;
            btnSub.Text = "Submit";
            btnSub.UseVisualStyleBackColor = true;
            btnSub.Click += btnSub_Click;
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(groupBox8);
            groupBox1.Controls.Add(groupBox7);
            groupBox1.Controls.Add(groupBox6);
            groupBox1.Controls.Add(label5);
            groupBox1.Location = new Point(12, 12);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new Size(1224, 150);
            groupBox1.TabIndex = 2;
            groupBox1.TabStop = false;
            // 
            // groupBox8
            // 
            groupBox8.Controls.Add(label2);
            groupBox8.Controls.Add(label6);
            groupBox8.Controls.Add(label7);
            groupBox8.Controls.Add(label8);
            groupBox8.Location = new Point(753, 22);
            groupBox8.Name = "groupBox8";
            groupBox8.Size = new Size(465, 112);
            groupBox8.TabIndex = 8;
            groupBox8.TabStop = false;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Segoe UI", 28F, FontStyle.Bold, GraphicsUnit.Point);
            label2.Location = new Point(6, 24);
            label2.Name = "label2";
            label2.Size = new Size(174, 74);
            label2.TabIndex = 1;
            label2.Text = "HSK1";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new Font("Segoe UI", 11F, FontStyle.Regular, GraphicsUnit.Point);
            label6.Location = new Point(186, 19);
            label6.Name = "label6";
            label6.Size = new Size(129, 30);
            label6.TabIndex = 3;
            label6.Text = "听力：20题";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Font = new Font("Segoe UI", 11F, FontStyle.Regular, GraphicsUnit.Point);
            label7.Location = new Point(186, 49);
            label7.Name = "label7";
            label7.Size = new Size(129, 30);
            label7.TabIndex = 4;
            label7.Text = "阅读：20题";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Font = new Font("Segoe UI", 11F, FontStyle.Regular, GraphicsUnit.Point);
            label8.Location = new Point(186, 79);
            label8.Name = "label8";
            label8.Size = new Size(158, 30);
            label8.TabIndex = 5;
            label8.Text = "时间：40 分钟";
            // 
            // groupBox7
            // 
            groupBox7.Controls.Add(label9);
            groupBox7.Controls.Add(btnEnter);
            groupBox7.Location = new Point(323, 22);
            groupBox7.Name = "groupBox7";
            groupBox7.Size = new Size(424, 112);
            groupBox7.TabIndex = 7;
            groupBox7.TabStop = false;
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Location = new Point(40, 27);
            label9.Name = "label9";
            label9.Size = new Size(284, 25);
            label9.TabIndex = 2;
            label9.Text = "Press the button to start listen test";
            // 
            // btnEnter
            // 
            btnEnter.Location = new Point(121, 59);
            btnEnter.Name = "btnEnter";
            btnEnter.Size = new Size(112, 34);
            btnEnter.TabIndex = 0;
            btnEnter.Text = "Start";
            btnEnter.UseVisualStyleBackColor = true;
            btnEnter.Click += btnEnter_Click;
            // 
            // groupBox6
            // 
            groupBox6.Controls.Add(label10);
            groupBox6.Controls.Add(label1);
            groupBox6.Location = new Point(6, 22);
            groupBox6.Name = "groupBox6";
            groupBox6.Size = new Size(311, 112);
            groupBox6.TabIndex = 6;
            groupBox6.TabStop = false;
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            label10.Location = new Point(12, 47);
            label10.Name = "label10";
            label10.Size = new Size(72, 32);
            label10.TabIndex = 3;
            label10.Text = "Time:";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new Font("Segoe UI", 14F, FontStyle.Regular, GraphicsUnit.Point);
            label5.Location = new Point(806, 57);
            label5.Name = "label5";
            label5.Size = new Size(0, 38);
            label5.TabIndex = 2;
            // 
            // groupBox2
            // 
            groupBox2.Controls.Add(btnSub);
            groupBox2.Location = new Point(724, 655);
            groupBox2.Name = "groupBox2";
            groupBox2.Size = new Size(512, 77);
            groupBox2.TabIndex = 3;
            groupBox2.TabStop = false;
            // 
            // groupBox3
            // 
            groupBox3.Controls.Add(pictureBox1);
            groupBox3.Controls.Add(txtQuestion);
            groupBox3.Location = new Point(12, 168);
            groupBox3.Name = "groupBox3";
            groupBox3.Size = new Size(706, 564);
            groupBox3.TabIndex = 4;
            groupBox3.TabStop = false;
            // 
            // pictureBox1
            // 
            pictureBox1.Location = new Point(6, 15);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.Size = new Size(694, 537);
            pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox1.TabIndex = 1;
            pictureBox1.TabStop = false;
            // 
            // txtQuestion
            // 
            txtQuestion.Location = new Point(18, 30);
            txtQuestion.Multiline = true;
            txtQuestion.Name = "txtQuestion";
            txtQuestion.ReadOnly = true;
            txtQuestion.Size = new Size(682, 522);
            txtQuestion.TabIndex = 0;
            // 
            // groupBox4
            // 
            groupBox4.Location = new Point(724, 168);
            groupBox4.Name = "groupBox4";
            groupBox4.Size = new Size(236, 488);
            groupBox4.TabIndex = 5;
            groupBox4.TabStop = false;
            // 
            // groupBox5
            // 
            groupBox5.Controls.Add(button22);
            groupBox5.Controls.Add(button23);
            groupBox5.Controls.Add(button24);
            groupBox5.Controls.Add(button25);
            groupBox5.Controls.Add(button26);
            groupBox5.Controls.Add(button27);
            groupBox5.Controls.Add(button28);
            groupBox5.Controls.Add(button29);
            groupBox5.Controls.Add(button30);
            groupBox5.Controls.Add(button31);
            groupBox5.Controls.Add(button32);
            groupBox5.Controls.Add(button33);
            groupBox5.Controls.Add(button34);
            groupBox5.Controls.Add(button35);
            groupBox5.Controls.Add(button36);
            groupBox5.Controls.Add(button37);
            groupBox5.Controls.Add(button38);
            groupBox5.Controls.Add(button39);
            groupBox5.Controls.Add(button40);
            groupBox5.Controls.Add(button41);
            groupBox5.Controls.Add(button18);
            groupBox5.Controls.Add(button19);
            groupBox5.Controls.Add(button20);
            groupBox5.Controls.Add(button21);
            groupBox5.Controls.Add(button14);
            groupBox5.Controls.Add(button15);
            groupBox5.Controls.Add(button16);
            groupBox5.Controls.Add(button17);
            groupBox5.Controls.Add(button10);
            groupBox5.Controls.Add(button11);
            groupBox5.Controls.Add(button12);
            groupBox5.Controls.Add(button13);
            groupBox5.Controls.Add(button6);
            groupBox5.Controls.Add(button7);
            groupBox5.Controls.Add(button8);
            groupBox5.Controls.Add(button9);
            groupBox5.Controls.Add(button5);
            groupBox5.Controls.Add(button4);
            groupBox5.Controls.Add(button3);
            groupBox5.Controls.Add(button2);
            groupBox5.Controls.Add(label4);
            groupBox5.Controls.Add(label3);
            groupBox5.Location = new Point(966, 168);
            groupBox5.Name = "groupBox5";
            groupBox5.Size = new Size(270, 488);
            groupBox5.TabIndex = 6;
            groupBox5.TabStop = false;
            // 
            // button22
            // 
            button22.Location = new Point(209, 417);
            button22.Name = "button22";
            button22.Size = new Size(42, 34);
            button22.TabIndex = 41;
            button22.Text = "40";
            button22.UseVisualStyleBackColor = true;
            button22.Click += Button_Click;
            // 
            // button23
            // 
            button23.Location = new Point(209, 377);
            button23.Name = "button23";
            button23.Size = new Size(42, 34);
            button23.TabIndex = 40;
            button23.Text = "35";
            button23.UseVisualStyleBackColor = true;
            button23.Click += Button_Click;
            // 
            // button24
            // 
            button24.Location = new Point(209, 337);
            button24.Name = "button24";
            button24.Size = new Size(42, 34);
            button24.TabIndex = 39;
            button24.Text = "30";
            button24.UseVisualStyleBackColor = true;
            button24.Click += Button_Click;
            // 
            // button25
            // 
            button25.Location = new Point(209, 297);
            button25.Name = "button25";
            button25.Size = new Size(42, 34);
            button25.TabIndex = 38;
            button25.Text = "25";
            button25.UseVisualStyleBackColor = true;
            button25.Click += Button_Click;
            // 
            // button26
            // 
            button26.Location = new Point(161, 417);
            button26.Name = "button26";
            button26.Size = new Size(42, 34);
            button26.TabIndex = 37;
            button26.Text = "39";
            button26.UseVisualStyleBackColor = true;
            button26.Click += Button_Click;
            // 
            // button27
            // 
            button27.Location = new Point(161, 377);
            button27.Name = "button27";
            button27.Size = new Size(42, 34);
            button27.TabIndex = 36;
            button27.Text = "34";
            button27.UseVisualStyleBackColor = true;
            button27.Click += Button_Click;
            // 
            // button28
            // 
            button28.Location = new Point(161, 337);
            button28.Name = "button28";
            button28.Size = new Size(42, 34);
            button28.TabIndex = 35;
            button28.Text = "29";
            button28.UseVisualStyleBackColor = true;
            button28.Click += Button_Click;
            // 
            // button29
            // 
            button29.Location = new Point(161, 297);
            button29.Name = "button29";
            button29.Size = new Size(42, 34);
            button29.TabIndex = 34;
            button29.Text = "24";
            button29.UseVisualStyleBackColor = true;
            button29.Click += Button_Click;
            // 
            // button30
            // 
            button30.Location = new Point(113, 417);
            button30.Name = "button30";
            button30.Size = new Size(42, 34);
            button30.TabIndex = 33;
            button30.Text = "38";
            button30.UseVisualStyleBackColor = true;
            button30.Click += Button_Click;
            // 
            // button31
            // 
            button31.Location = new Point(113, 377);
            button31.Name = "button31";
            button31.Size = new Size(42, 34);
            button31.TabIndex = 32;
            button31.Text = "33";
            button31.UseVisualStyleBackColor = true;
            button31.Click += Button_Click;
            // 
            // button32
            // 
            button32.Location = new Point(113, 337);
            button32.Name = "button32";
            button32.Size = new Size(42, 34);
            button32.TabIndex = 31;
            button32.Text = "28";
            button32.UseVisualStyleBackColor = true;
            button32.Click += Button_Click;
            // 
            // button33
            // 
            button33.Location = new Point(113, 297);
            button33.Name = "button33";
            button33.Size = new Size(42, 34);
            button33.TabIndex = 30;
            button33.Text = "23";
            button33.UseVisualStyleBackColor = true;
            button33.Click += Button_Click;
            // 
            // button34
            // 
            button34.Location = new Point(65, 417);
            button34.Name = "button34";
            button34.Size = new Size(42, 34);
            button34.TabIndex = 29;
            button34.Text = "37";
            button34.UseVisualStyleBackColor = true;
            button34.Click += Button_Click;
            // 
            // button35
            // 
            button35.Location = new Point(65, 377);
            button35.Name = "button35";
            button35.Size = new Size(42, 34);
            button35.TabIndex = 28;
            button35.Text = "32";
            button35.UseVisualStyleBackColor = true;
            button35.Click += Button_Click;
            // 
            // button36
            // 
            button36.Location = new Point(65, 337);
            button36.Name = "button36";
            button36.Size = new Size(42, 34);
            button36.TabIndex = 27;
            button36.Text = "27";
            button36.UseVisualStyleBackColor = true;
            button36.Click += Button_Click;
            // 
            // button37
            // 
            button37.Location = new Point(65, 297);
            button37.Name = "button37";
            button37.Size = new Size(42, 34);
            button37.TabIndex = 26;
            button37.Text = "22";
            button37.UseVisualStyleBackColor = true;
            button37.Click += Button_Click;
            // 
            // button38
            // 
            button38.Location = new Point(17, 417);
            button38.Name = "button38";
            button38.Size = new Size(42, 34);
            button38.TabIndex = 25;
            button38.Text = "36";
            button38.UseVisualStyleBackColor = true;
            button38.Click += Button_Click;
            // 
            // button39
            // 
            button39.Location = new Point(17, 377);
            button39.Name = "button39";
            button39.Size = new Size(42, 34);
            button39.TabIndex = 24;
            button39.Text = "31";
            button39.UseVisualStyleBackColor = true;
            button39.Click += Button_Click;
            // 
            // button40
            // 
            button40.Location = new Point(17, 337);
            button40.Name = "button40";
            button40.Size = new Size(42, 34);
            button40.TabIndex = 23;
            button40.Text = "26";
            button40.UseVisualStyleBackColor = true;
            button40.Click += Button_Click;
            // 
            // button41
            // 
            button41.Location = new Point(17, 297);
            button41.Name = "button41";
            button41.Size = new Size(42, 34);
            button41.TabIndex = 22;
            button41.Text = "21";
            button41.UseVisualStyleBackColor = true;
            button41.Click += Button_Click;
            // 
            // button18
            // 
            button18.Location = new Point(209, 198);
            button18.Name = "button18";
            button18.Size = new Size(42, 34);
            button18.TabIndex = 21;
            button18.Text = "20";
            button18.UseVisualStyleBackColor = true;
            button18.Click += Button_Click;
            // 
            // button19
            // 
            button19.Location = new Point(209, 158);
            button19.Name = "button19";
            button19.Size = new Size(42, 34);
            button19.TabIndex = 20;
            button19.Text = "15";
            button19.UseVisualStyleBackColor = true;
            button19.Click += Button_Click;
            // 
            // button20
            // 
            button20.Location = new Point(209, 118);
            button20.Name = "button20";
            button20.Size = new Size(42, 34);
            button20.TabIndex = 19;
            button20.Text = "10";
            button20.UseVisualStyleBackColor = true;
            button20.Click += Button_Click;
            // 
            // button21
            // 
            button21.Location = new Point(209, 78);
            button21.Name = "button21";
            button21.Size = new Size(42, 34);
            button21.TabIndex = 18;
            button21.Text = "5";
            button21.UseVisualStyleBackColor = true;
            button21.Click += Button_Click;
            // 
            // button14
            // 
            button14.Location = new Point(161, 198);
            button14.Name = "button14";
            button14.Size = new Size(42, 34);
            button14.TabIndex = 17;
            button14.Text = "19";
            button14.UseVisualStyleBackColor = true;
            button14.Click += Button_Click;
            // 
            // button15
            // 
            button15.Location = new Point(161, 158);
            button15.Name = "button15";
            button15.Size = new Size(42, 34);
            button15.TabIndex = 16;
            button15.Text = "14";
            button15.UseVisualStyleBackColor = true;
            button15.Click += Button_Click;
            // 
            // button16
            // 
            button16.Location = new Point(161, 118);
            button16.Name = "button16";
            button16.Size = new Size(42, 34);
            button16.TabIndex = 15;
            button16.Text = "9";
            button16.UseVisualStyleBackColor = true;
            button16.Click += Button_Click;
            // 
            // button17
            // 
            button17.Location = new Point(161, 78);
            button17.Name = "button17";
            button17.Size = new Size(42, 34);
            button17.TabIndex = 14;
            button17.Text = "4";
            button17.UseVisualStyleBackColor = true;
            button17.Click += Button_Click;
            // 
            // button10
            // 
            button10.Location = new Point(113, 198);
            button10.Name = "button10";
            button10.Size = new Size(42, 34);
            button10.TabIndex = 13;
            button10.Text = "18";
            button10.UseVisualStyleBackColor = true;
            button10.Click += Button_Click;
            // 
            // button11
            // 
            button11.Location = new Point(113, 158);
            button11.Name = "button11";
            button11.Size = new Size(42, 34);
            button11.TabIndex = 12;
            button11.Text = "13";
            button11.UseVisualStyleBackColor = true;
            button11.Click += Button_Click;
            // 
            // button12
            // 
            button12.Location = new Point(113, 118);
            button12.Name = "button12";
            button12.Size = new Size(42, 34);
            button12.TabIndex = 11;
            button12.Text = "8";
            button12.UseVisualStyleBackColor = true;
            button12.Click += Button_Click;
            // 
            // button13
            // 
            button13.Location = new Point(113, 78);
            button13.Name = "button13";
            button13.Size = new Size(42, 34);
            button13.TabIndex = 10;
            button13.Text = "3";
            button13.UseVisualStyleBackColor = true;
            button13.Click += Button_Click;
            // 
            // button6
            // 
            button6.Location = new Point(65, 198);
            button6.Name = "button6";
            button6.Size = new Size(42, 34);
            button6.TabIndex = 9;
            button6.Text = "17";
            button6.UseVisualStyleBackColor = true;
            button6.Click += Button_Click;
            // 
            // button7
            // 
            button7.Location = new Point(65, 158);
            button7.Name = "button7";
            button7.Size = new Size(42, 34);
            button7.TabIndex = 8;
            button7.Text = "12";
            button7.UseVisualStyleBackColor = true;
            button7.Click += Button_Click;
            // 
            // button8
            // 
            button8.Location = new Point(65, 118);
            button8.Name = "button8";
            button8.Size = new Size(42, 34);
            button8.TabIndex = 7;
            button8.Text = "7";
            button8.UseVisualStyleBackColor = true;
            button8.Click += Button_Click;
            // 
            // button9
            // 
            button9.Location = new Point(65, 78);
            button9.Name = "button9";
            button9.Size = new Size(42, 34);
            button9.TabIndex = 6;
            button9.Text = "2";
            button9.UseVisualStyleBackColor = true;
            button9.Click += Button_Click;
            // 
            // button5
            // 
            button5.Location = new Point(17, 198);
            button5.Name = "button5";
            button5.Size = new Size(42, 34);
            button5.TabIndex = 5;
            button5.Text = "16";
            button5.UseVisualStyleBackColor = true;
            button5.Click += Button_Click;
            // 
            // button4
            // 
            button4.Location = new Point(17, 158);
            button4.Name = "button4";
            button4.Size = new Size(42, 34);
            button4.TabIndex = 4;
            button4.Text = "11";
            button4.UseVisualStyleBackColor = true;
            button4.Click += Button_Click;
            // 
            // button3
            // 
            button3.Location = new Point(17, 118);
            button3.Name = "button3";
            button3.Size = new Size(42, 34);
            button3.TabIndex = 3;
            button3.Text = "6";
            button3.UseVisualStyleBackColor = true;
            button3.Click += Button_Click;
            // 
            // button2
            // 
            button2.Location = new Point(17, 78);
            button2.Name = "button2";
            button2.Size = new Size(42, 34);
            button2.TabIndex = 2;
            button2.Text = "1";
            button2.UseVisualStyleBackColor = true;
            button2.Click += Button_Click;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new Font("Segoe UI", 11F, FontStyle.Bold, GraphicsUnit.Point);
            label4.Location = new Point(6, 246);
            label4.Name = "label4";
            label4.Size = new Size(61, 30);
            label4.TabIndex = 1;
            label4.Text = "阅读";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new Font("Segoe UI", 11F, FontStyle.Bold, GraphicsUnit.Point);
            label3.Location = new Point(6, 30);
            label3.Name = "label3";
            label3.Size = new Size(61, 30);
            label3.TabIndex = 0;
            label3.Text = "听力";
            // 
            // Form3_3_3_1
            // 
            AutoScaleDimensions = new SizeF(10F, 25F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1248, 751);
            Controls.Add(groupBox5);
            Controls.Add(groupBox4);
            Controls.Add(groupBox3);
            Controls.Add(groupBox2);
            Controls.Add(groupBox1);
            Name = "Form3_3_3_1";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Form3_3_3_1";
            Load += Form3_3_3_1_Load;
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            groupBox8.ResumeLayout(false);
            groupBox8.PerformLayout();
            groupBox7.ResumeLayout(false);
            groupBox7.PerformLayout();
            groupBox6.ResumeLayout(false);
            groupBox6.PerformLayout();
            groupBox2.ResumeLayout(false);
            groupBox3.ResumeLayout(false);
            groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).EndInit();
            groupBox5.ResumeLayout(false);
            groupBox5.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private Label label1;
        private Button btnSub;
        private GroupBox groupBox1;
        private Label label2;
        private GroupBox groupBox2;
        private GroupBox groupBox3;
        private GroupBox groupBox4;
        private GroupBox groupBox5;
        private Button button2;
        private Label label4;
        private Label label3;
        private TextBox txtQuestion;
        private PictureBox pictureBox1;
        private Button button22;
        private Button button23;
        private Button button24;
        private Button button25;
        private Button button26;
        private Button button27;
        private Button button28;
        private Button button29;
        private Button button30;
        private Button button31;
        private Button button32;
        private Button button33;
        private Button button34;
        private Button button35;
        private Button button36;
        private Button button37;
        private Button button38;
        private Button button39;
        private Button button40;
        private Button button41;
        private Button button18;
        private Button button19;
        private Button button20;
        private Button button21;
        private Button button14;
        private Button button15;
        private Button button16;
        private Button button17;
        private Button button10;
        private Button button11;
        private Button button12;
        private Button button13;
        private Button button6;
        private Button button7;
        private Button button8;
        private Button button9;
        private Button button5;
        private Button button4;
        private Button button3;
        private Label label5;
        private Label label6;
        private Label label8;
        private Label label7;
        private GroupBox groupBox6;
        private GroupBox groupBox8;
        private GroupBox groupBox7;
        private Label label9;
        private Button btnEnter;
        private Label label10;
    }
}