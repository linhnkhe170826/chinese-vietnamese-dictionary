﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dictionary
{
    internal class Answer
    {
        public int AnswerId { get; set; }
        public int QuestionId { get; set; }
        public string AnswerText { get; set; }
        public Answer() { }

        public Answer(int answerId, int questionId, string answerText)
        {
            AnswerId = answerId;
            QuestionId = questionId;
            AnswerText = answerText;
        }
        static String GetConnectionString()
        {
            IConfiguration config = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", true, true).Build();
            var strConnection = config.GetConnectionString("MyDB");
            return strConnection;
        }
        public List<Answer> GetQuestion(int questionId)
        {
            List<Answer> answers = new List<Answer>();
            String ConnectionString = GetConnectionString();
            SqlConnection connection = new SqlConnection(ConnectionString);
            string Query = "   Select * from Answers where QuestionID = @QuesId";
            SqlCommand command = new SqlCommand(Query, connection);
            command.Parameters.AddWithValue("@QuesId", questionId);
            try
            {
                connection.Open();
                SqlCommand sqlCommand = new SqlCommand(Query, connection);
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        answers.Add(new Answer
                        {
                            AnswerId = reader.GetInt32("AnswerID"),
                            QuestionId = reader.GetInt32("QuestionID"),
                            AnswerText = reader.GetString("AnswerText")
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally { connection.Close(); }
            return answers;
        }
    }
}
