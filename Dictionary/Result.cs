﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dictionary
{
    internal class Result
    {
        public int ResultId { get; set; }
        public int Score { get; set; }
        public DateTime DateTaken { get; set; }

        public Result()
        {
        }

        public Result(int resultId, int score, DateTime dateTaken)
        {
            ResultId = resultId;
            Score = score;
            DateTaken = dateTaken;
        }

        SqlConnection connection;
        SqlCommand command;
        static String GetConnectionString()
        {
            IConfiguration config = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", true, true).Build();
            var strConnection = config.GetConnectionString("MyDB");
            return strConnection;
        }
        public List<Result> FindAllResult(int ExamId)
        {
            List<Result> results = new List<Result>();
            connection = new SqlConnection(GetConnectionString());
            string Query = "  select * from Results where ExamID = @ExamID";
            command = new SqlCommand(Query, connection);
            command.Parameters.AddWithValue("@ExamID", ExamId);
            try
            {
                connection.Open();
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        results.Add(new Result
                        {
                            ResultId = reader.GetInt32("ResultID"),
                            Score = reader.GetInt32("Score"),
                            DateTaken = reader.GetDateTime("DateTaken")
                        });
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally { connection.Close(); }
            return results;
        }
    }
}
