﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dictionary
{
    public partial class Form2_2 : Form
    {
        string rootDirectory = Directory.GetParent(Directory.GetParent(Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.FullName).Parent.FullName).Parent.FullName;
        public Form2_2()
        {
            InitializeComponent();
        }
        Practice practice = new Practice();
        List<Practice> list = new List<Practice>();
        string currentWord;
        string currentMean;
        int index = 0, score = 0;
        Random random = new Random();
        private void Form2_2_Load(object sender, EventArgs e)
        {
            LoadNextWord();
        }

        private void LoadNextWord()
        {
            list = practice.GetPractices();
            if (index < list.Count)
            {
                currentWord = list[index].word;
                currentMean = list[index].mean;
                txtMean.Text = currentMean;
                txtWord.Text = ""; // Clear the guess textbox
            }
            else
            {
                MessageBox.Show($"Your corect answer: {score}", "Result", MessageBoxButtons.OK);
                DialogResult result = MessageBox.Show("No more words to display!", "Replay", MessageBoxButtons.RetryCancel);
                if (result == DialogResult.Cancel)
                {
                    FrmPractice frmPractice = new FrmPractice();
                    frmPractice.FormClosed += FrmPractice_FormClosed; //Đóng cái frmPractice lại nè
                    frmPractice.Show(); //Hiển thị from Practice
                    this.Hide();
                }
                else
                {
                    Form2_2 form2_2 = new Form2_2();
                    form2_2.FormClosed += Form2_2_FormClosed;
                    form2_2.Show();
                    this.Hide();
                }

            }
        }

        private void btnSub_Click(object sender, EventArgs e)
        {
            string word = txtWord.Text.Trim();
            bool correctGuess = (word.ToLower() == currentWord.ToLower());
            if (correctGuess)
            {
                pictureBox1.Load(rootDirectory+"\\Dictionary\\Picturer\\Yes.jpg");
                score += 1;
            }
            else
            {
                MessageBox.Show($"Corect answer: {currentWord}");
                pictureBox1.Load(rootDirectory + "\\Dictionary\\Picturer\\No.jpg");
            }
            index++;
            LoadNextWord();

            // Update the score display
            lbScore.Text = score.ToString();
        }
        private void FrmPractice_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close(); // Đóng form FrmSearchWord khi form FrmPractice được đóng
        }
        private void Form2_2_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close(); // Đóng form FrmSearchWord khi form FrmTest được đóng
        }
    }
}

