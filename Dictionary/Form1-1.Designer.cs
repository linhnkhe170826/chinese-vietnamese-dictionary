﻿namespace Dictionary
{
    partial class Form1_1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            txtMean = new TextBox();
            txtWord = new TextBox();
            btnSave = new Button();
            groupBox1 = new GroupBox();
            lbSearchWord = new Label();
            groupBox2 = new GroupBox();
            groupBox3 = new GroupBox();
            groupBox1.SuspendLayout();
            groupBox2.SuspendLayout();
            groupBox3.SuspendLayout();
            SuspendLayout();
            // 
            // txtMean
            // 
            txtMean.Location = new Point(6, 20);
            txtMean.Multiline = true;
            txtMean.Name = "txtMean";
            txtMean.ScrollBars = ScrollBars.Vertical;
            txtMean.Size = new Size(631, 215);
            txtMean.TabIndex = 0;
            // 
            // txtWord
            // 
            txtWord.Font = new Font("Segoe UI", 16F, FontStyle.Regular, GraphicsUnit.Point);
            txtWord.Location = new Point(6, 71);
            txtWord.Name = "txtWord";
            txtWord.ReadOnly = true;
            txtWord.Size = new Size(631, 50);
            txtWord.TabIndex = 1;
            // 
            // btnSave
            // 
            btnSave.Location = new Point(285, 390);
            btnSave.Name = "btnSave";
            btnSave.Size = new Size(112, 34);
            btnSave.TabIndex = 2;
            btnSave.Text = "Save";
            btnSave.UseVisualStyleBackColor = true;
            btnSave.Click += btnSave_Click;
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(lbSearchWord);
            groupBox1.Controls.Add(txtWord);
            groupBox1.Location = new Point(18, 12);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new Size(643, 133);
            groupBox1.TabIndex = 3;
            groupBox1.TabStop = false;
            // 
            // lbSearchWord
            // 
            lbSearchWord.AutoSize = true;
            lbSearchWord.Font = new Font("Segoe UI Semibold", 16F, FontStyle.Bold, GraphicsUnit.Point);
            lbSearchWord.Location = new Point(23, 10);
            lbSearchWord.Name = "lbSearchWord";
            lbSearchWord.Size = new Size(208, 45);
            lbSearchWord.TabIndex = 20;
            lbSearchWord.Text = "Thêm từ mới";
            // 
            // groupBox2
            // 
            groupBox2.Controls.Add(txtMean);
            groupBox2.Location = new Point(6, 139);
            groupBox2.Name = "groupBox2";
            groupBox2.Size = new Size(643, 241);
            groupBox2.TabIndex = 4;
            groupBox2.TabStop = false;
            // 
            // groupBox3
            // 
            groupBox3.Controls.Add(groupBox2);
            groupBox3.Location = new Point(12, 4);
            groupBox3.Name = "groupBox3";
            groupBox3.Size = new Size(655, 434);
            groupBox3.TabIndex = 5;
            groupBox3.TabStop = false;
            // 
            // Form1_1
            // 
            AutoScaleDimensions = new SizeF(10F, 25F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(679, 450);
            Controls.Add(groupBox1);
            Controls.Add(btnSave);
            Controls.Add(groupBox3);
            Name = "Form1_1";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Form1_1";
            Load += Form1_1_Load;
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            groupBox2.ResumeLayout(false);
            groupBox2.PerformLayout();
            groupBox3.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private TextBox txtMean;
        private TextBox txtWord;
        private Button btnSave;
        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private GroupBox groupBox3;
        private Label lbSearchWord;
    }
}