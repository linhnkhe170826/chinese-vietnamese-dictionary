﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dictionary
{
    public partial class Form3_3_3_1 : Form
    {
        string rootDirectory = Directory.GetParent(Directory.GetParent(Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.FullName).Parent.FullName).Parent.FullName;
        int index;
        int examId;
        Question question = new Question();
        Answer answer = new Answer();
        List<Answer> answerList;
        List<Question> questionList;
        StudentRespone studentRespone = new StudentRespone();
        int resultid = 0;
        int second = 2389; //40 phut
        //int second = 10;
        SoundPlayer player = new SoundPlayer();
        public Form3_3_3_1()
        {
            InitializeComponent();
        }
        public Form3_3_3_1(int Exid)
        {
            examId = Exid;
            InitializeComponent();
            timer1.Start();
        }

        private void btnEnter_Click(object sender, EventArgs e)
        {
            Exams exams = new Exams();
            /*WMPLib.WindowsMediaPlayer wplayer = new WMPLib.WindowsMediaPlayer();
            wplayer.URL = "D:\\PRN211\\Dictionary\\Exam\\HSK1\\1.mp3";
            wplayer.controls.play();*/
            //player.SoundLocation = rootDirectory + @"\Dictionary\Exam\HSK1\1.wav";
            player.SoundLocation = rootDirectory + exams.GetListen(examId);
            player.Play();
        }

        private void Button_Click(object sender, EventArgs e)
        {
            btnSub.Enabled = true;
            Button clickedButton = sender as Button; // Lấy ra nút đã được nhấn

            // Kiểm tra xem nút đã được nhấn có phải là một Button không
            if (clickedButton != null)
            {
                index = Int32.Parse(clickedButton.Text); // Lấy văn bản của nút đã nhấn
                LoadQuestion(index - 1);
                //MessageBox.Show(index.ToString());
            }
        }
        public void LoadQuestion(int index)
        {
            questionList = question.GetQuestion(examId);
            string questionText = questionList[index].QuestionText;
            int questionId = questionList[index].QuestionId;
            if (questionText.Contains(".PNG"))
            {
                txtQuestion.Hide();
                pictureBox1.Show();
                pictureBox1.Enabled = true;
                pictureBox1.Load(rootDirectory + questionText);
            }
            else
            {
                pictureBox1.Hide();
                txtQuestion.Show();
                txtQuestion.Text = questionText;
            }
            LoadAnswer(questionId);
        }
        public void LoadAnswer(int questionID)
        {
            groupBox4.Controls.Clear();
            answerList = answer.GetQuestion(questionID);
            int radioButtonTop = 48;
            StudentRespone respone = studentRespone.FindResspone(questionID, resultid);
            foreach (var answerItem in answerList)
            {
                RadioButton radioButton = new RadioButton();
                radioButton.AutoSize = true;
                radioButton.Location = new Point(46, radioButtonTop);
                radioButton.Size = new Size(141, 29);
                radioButton.TabIndex = groupBox4.Controls.Count; // Sử dụng số lượng control hiện tại trong groupBox4 làm TabIndex
                radioButton.TabStop = true;
                radioButton.Text = answerItem.AnswerText;
                radioButton.UseVisualStyleBackColor = true;
                radioButton.CheckedChanged += radioBution_CheckedChange;
                // Thêm RadioButton vào groupBox4
                groupBox4.Controls.Add(radioButton);
                if (respone != null && answerItem.AnswerText.Equals(respone.SelectedAnswer))
                {
                    radioButton.Checked = true;
                }
                // Tăng vị trí top cho RadioButton tiếp theo
                radioButtonTop += radioButton.Height + 5;
            }

        }

        private void Form3_3_3_1_Load(object sender, EventArgs e)
        {
            pictureBox1.Hide();
            txtQuestion.Hide();
            label1.Text = "40:00";
            resultid = studentRespone.AddResult(examId);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            second--;
            label1.Text = ConvertToMinues(second);
            timer1.Interval = 1000;
            if (second <= 0)
            {
                timer1.Stop();
                MessageBox.Show("Time over");
                ShowResult();
            }
        }

        private string ConvertToMinues(int second)
        {
            string con = "";
            int min = 0;
            if (second >= 60)
            {
                min = second / 60;
                second %= 60;
            }
            string minStr = (min < 10) ? "0" + min : min.ToString();
            string secStr = (second < 10) ? "0" + second : second.ToString();

            con = minStr + ":" + secStr;
            return con;
        }
        private void radioBution_CheckedChange(object sender, EventArgs e)
        {
            RadioButton radioButton = sender as RadioButton;
            if (radioButton != null)
            {
                StudentRespone respone = studentRespone.FindResspone(index, resultid);
                if (respone != null)
                {
                    studentRespone.UpdateResspone(new StudentRespone(respone.ResponsiveId, respone.ResultId, respone.QuestionId, radioButton.Text));
                }
                else studentRespone.AddResspone(new StudentRespone(resultid, index, radioButton.Text));
            }

        }

        private void btnSub_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            ShowResult();
        }
        public int Score()
        {
            int score = 0;
            questionList = question.GetQuestion(examId);
            StudentRespone respone ;
            foreach (var question in questionList)
            {
                respone = studentRespone.FindResspone(question.QuestionId, resultid);
                if (respone!=null && question.CorrectAnswer.Equals(respone.SelectedAnswer))
                {
                    score++;
                }
            }
            studentRespone.UpdateResult(score, resultid);
            return score;
        }
        public void ShowResult()
        {
            player.Stop();
            int score = Score();
            int number = questionList.Count;
            if (score / number >= 0.8)
            {
                MessageBox.Show($"Your score is: {score} / {number}", "Passed");
            }
            else MessageBox.Show($"Your score is: {score} / {number}", "Failed");
            FrmTest frmTest = new FrmTest();
            frmTest.FormClosed += FrmTest_FormClosed;
            frmTest.Show();
            this.Hide();
        }
        private void FrmTest_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close(); // Đóng form FrmSearchWord khi form FrmTest được đóng
        }
    }
}
