﻿namespace Dictionary
{
    partial class Form3_3_3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            label1 = new Label();
            btnAdd = new Button();
            btnBack = new Button();
            btnClose = new Button();
            dataGridView1 = new DataGridView();
            btnSort = new Button();
            ((System.ComponentModel.ISupportInitialize)dataGridView1).BeginInit();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 14F, FontStyle.Bold, GraphicsUnit.Point);
            label1.Location = new Point(238, 19);
            label1.Name = "label1";
            label1.Size = new Size(135, 38);
            label1.TabIndex = 37;
            label1.Text = "All result";
            // 
            // btnAdd
            // 
            btnAdd.Location = new Point(351, 332);
            btnAdd.Name = "btnAdd";
            btnAdd.Size = new Size(136, 59);
            btnAdd.TabIndex = 36;
            btnAdd.Text = "Do new test";
            btnAdd.UseVisualStyleBackColor = true;
            btnAdd.Click += btnAdd_Click;
            // 
            // btnBack
            // 
            btnBack.Location = new Point(34, 416);
            btnBack.Name = "btnBack";
            btnBack.Size = new Size(121, 34);
            btnBack.TabIndex = 35;
            btnBack.Text = "Back";
            btnBack.UseVisualStyleBackColor = true;
            btnBack.Click += btnBack_Click;
            // 
            // btnClose
            // 
            btnClose.Location = new Point(468, 416);
            btnClose.Name = "btnClose";
            btnClose.Size = new Size(121, 34);
            btnClose.TabIndex = 34;
            btnClose.Text = "Close";
            btnClose.UseVisualStyleBackColor = true;
            btnClose.Click += btnClose_Click;
            // 
            // dataGridView1
            // 
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridView1.Location = new Point(69, 81);
            dataGridView1.Name = "dataGridView1";
            dataGridView1.ReadOnly = true;
            dataGridView1.RowHeadersWidth = 62;
            dataGridView1.RowTemplate.Height = 33;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.Size = new Size(470, 228);
            dataGridView1.TabIndex = 33;
            // 
            // btnSort
            // 
            btnSort.Location = new Point(118, 332);
            btnSort.Name = "btnSort";
            btnSort.Size = new Size(155, 59);
            btnSort.TabIndex = 38;
            btnSort.Text = "Sort descending by score";
            btnSort.UseVisualStyleBackColor = true;
            btnSort.Click += btnSort_Click;
            // 
            // Form3_3_3
            // 
            AutoScaleDimensions = new SizeF(10F, 25F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(628, 462);
            Controls.Add(btnSort);
            Controls.Add(label1);
            Controls.Add(btnAdd);
            Controls.Add(btnBack);
            Controls.Add(btnClose);
            Controls.Add(dataGridView1);
            Name = "Form3_3_3";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Form3_3_3";
            Load += Form3_3_3_Load;
            ((System.ComponentModel.ISupportInitialize)dataGridView1).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label label1;
        private Button btnAdd;
        private Button btnBack;
        private Button btnClose;
        private DataGridView dataGridView1;
        private Button btnSort;
    }
}