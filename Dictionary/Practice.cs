﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Dictionary
{
    public class Practice
    {
        public string word { get; set; }
        public string mean { get; set; }
        public int dificult { get; set; }

        public Practice(string word, string mean)
        {
            this.word = word;
            this.mean = mean;
        }

        public Practice()
        {
        }

        public Practice(string word, string mean, int dificult)
        {
            this.word = word;
            this.mean = mean;
            this.dificult = dificult;
        }
        SqlConnection connection;
        SqlCommand command;
        static String GetConnectionString()
        {
            IConfiguration config = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", true, true).Build();
            var strConnection = config.GetConnectionString("MyDB");
            return strConnection;
        }
        public void InsertWord(Practice practice)
        {
            connection = new SqlConnection(GetConnectionString());
            string Query = "INSERT INTO Practice VALUES (@Word,@Mean, @Dificult);";
            command = new SqlCommand(Query, connection);
            command.Parameters.AddWithValue("@Word", practice.word); // Assuming Word is a property of Practice
            command.Parameters.AddWithValue("@Mean", practice.mean); // Assuming Meaning is a property of Practice
            command.Parameters.AddWithValue("@Dificult", practice.dificult); // Assuming dificult is a property of Practice
            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally { connection.Close(); }
        }
        public void DeleteWord(string word)
        {
            connection = new SqlConnection(GetConnectionString());
            string Query = "delete from Practice where Word = @Word";
            command = new SqlCommand(Query, connection);
            command.Parameters.AddWithValue("@Word", word); 
            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally { connection.Close(); }
        }

        public bool CheckExist(string word)
        {
            string connectionString = GetConnectionString();

            // SQL query to check if the word exists in a specific table
            string query = "SELECT COUNT(*) FROM Practice WHERE Word = @Word";

            // Create SqlConnection and SqlCommand objects
            SqlConnection connection = new SqlConnection(connectionString);
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                // Add parameter for the word to search
                command.Parameters.AddWithValue("@Word", word);

                // Open the connection
                connection.Open();

                // Execute the query and get the result
                int count = (int)command.ExecuteScalar();

                // Check if the word exists or not
                if (count == 0) //not exist return true;
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public List<Practice> GetPractices()
        {
            List<Practice> practices = new List<Practice>();
            String ConnectionString = GetConnectionString();
            SqlConnection connection = new SqlConnection(ConnectionString);
            string Query = "Select * from Practice";
            SqlCommand command = new SqlCommand(Query, connection);
            try
            {
                connection.Open();
                SqlCommand sqlCommand = new SqlCommand(Query, connection);
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        practices.Add(new Practice
                        {
                            word = reader.GetString("Word"),
                            mean = reader.GetString("Mean"),
                            dificult = reader.GetInt32("Dificult")
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally { connection.Close(); }
            return practices;
        }
        public List<string> GetDificult()
        {
            List<string> dificult = new List<string>();
            String ConnectionString = GetConnectionString();
            SqlConnection connection = new SqlConnection(ConnectionString);
            string Query = "select DISTINCT Dificult from Practice";
            SqlCommand command = new SqlCommand(Query, connection);
            try
            {
                connection.Open();
                SqlCommand sqlCommand = new SqlCommand(Query, connection);
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        dificult.Add(reader.GetInt32("Dificult").ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally { connection.Close(); }
            return dificult;
        }
    }
}
