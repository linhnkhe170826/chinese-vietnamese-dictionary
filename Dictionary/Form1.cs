﻿
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.CodeDom;
using System.Data;
using System.Text.RegularExpressions;

namespace Dictionary
{
    public partial class FrmSearchWord : Form
    {
        int type;
        string rootDirectory = Directory.GetParent(Directory.GetParent(Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.FullName).Parent.FullName).Parent.FullName;
        Practice Practice = new Practice();
        public FrmSearchWord()
        {
            InitializeComponent();
        }

        // Load form tra từ nè
        public void FrmSearchWord_Load(object sender, EventArgs e)
        {
            txtSearch.Text = string.Empty;
            txtMean.Text = string.Empty;
            btnUpdateInformation.Enabled = false;
            btnDeleteInformation.Enabled = false;
            btnAddPractice.Enabled = false;
        }
        public void SetOpen()
        {
            btnUpdateInformation.Enabled = true;
            btnDeleteInformation.Enabled = true;
            Practice practice = new Practice();
            if(practice.CheckExist(txtSearch.Text))
            {
                btnAddPractice.Enabled = true;
            } else btnAddPractice.Enabled=false;
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void lbSearchWord_Click(object sender, EventArgs e)
        {
            FrmSearchWord_Load(sender, e);
        }

        private void lbPractice_Click(object sender, EventArgs e)
        {
            FrmPractice frmPractice = new FrmPractice();
            frmPractice.FormClosed += FrmPractice_FormClosed; //Đóng cái frmPractice lại nè
            frmPractice.Show(); //Hiển thị from Practice
            this.Hide(); // Ẩn form FrmSearchWord khi mở form FrmPractice
        }

        private void FrmPractice_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close(); // Đóng form FrmSearchWord khi form FrmPractice được đóng
        }

        private void lbTest_Click(object sender, EventArgs e)
        {
            FrmTest frmTest = new FrmTest();
            frmTest.FormClosed += FrmTest_FormClosed;
            frmTest.Show();
            this.Hide(); // Ẩn form FrmSearchWord khi mở form FrmTest
        }
        private void FrmTest_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close(); // Đóng form FrmSearchWord khi form FrmTest được đóng
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtSearch.Text = string.Empty;
            txtMean.Text = string.Empty;
        }

        private void btnSearchPinyin_Click(object sender, EventArgs e)
        {
            txtMean.Text = string.Empty;
            //string filePath = rootDirectory + @"\Dictionary\Pinyin.txt";
            string filePath = rootDirectory + @"\Dictionary\Pinyin.txt";
            type = 0;
            string word = txtSearch.Text;
            bool found = false;
            try
            {
                if (word == "") throw new Exception();
                // Khởi tạo một đối tượng StreamReader để đọc từ file
                using (StreamReader sr = new StreamReader(filePath))
                {
                    string line;
                    // Đọc từng dòng từ file cho đến khi kết thúc file
                    while ((line = sr.ReadLine()) != null)
                    {
                        string[] lineWord = line.Split('=');
                        if (lineWord[0].Equals(word))
                        {
                            txtMean.Text += lineWord[1].Replace("\\n\\t", Environment.NewLine).Replace("\\n✚", Environment.NewLine + '✚') + Environment.NewLine;
                            //MessageBox.Show(lineWord[1].Replace("\\n\\t", Environment.NewLine));
                            found = true;
                            break;
                        }
                    }
                    if (!found) txtMean.Text = "Không tìm thấy từ này";
                    SetOpen();
                }
            }
            catch (Exception ex)
            {
                // Xử lý các ngoại lệ nếu có
                Console.WriteLine(ex.Message);
            }
        }

        private void btnUpdateInformation_Click(object sender, EventArgs e)
        {
            if (txtMean.Text != "Không tìm thấy từ này")
            {
                Form1_1 form1_1 = new Form1_1(txtSearch.Text, txtMean.Text, type);
                form1_1.Show();
            }
            else
            {
                Form1_1 form1_1 = new Form1_1(txtSearch.Text, "", type);
                form1_1.Show();
            }
            txtSearch.Text = string.Empty;
            txtMean.Text = string.Empty;
            btnUpdateInformation.Enabled = false;
        }

        private void btnDeleteInformation_Click(object sender, EventArgs e)
        {
            string filePath;
            if (type == 0)
            {
                filePath = rootDirectory + @"\Dictionary\Pinyin.txt";
                //filePath = rootDirectory + @"\Dictionary\Pinyin.txt";
                //filePath = rootDirectory + @"\Dictionary\TestGhi1.txt";
            }
            else filePath = rootDirectory + @"\Dictionary\Han_Viet.txt";

            try
            {
                // Đọc toàn bộ nội dung của file vào bộ nhớ
                string[] lines = File.ReadAllLines(filePath);

                // Ghi đè dữ liệu mới
                using (StreamWriter sw = new StreamWriter(filePath))
                {
                    // Ghi đè dữ liệu mới vào file
                    foreach (string line in lines)
                    {
                        if (line.StartsWith(txtSearch.Text) || line.EndsWith(txtSearch.Text))
                        {
                            sw.WriteLine(""); // Ghi đè đoạn dữ liệu mới
                        }
                        else
                        {
                            sw.WriteLine(line); // Ghi lại dòng gốc nếu không phải dòng cần ghi đè
                        }
                    }
                }
                txtSearch.Text = string.Empty;
                txtMean.Text = string.Empty;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (txtSearch.Text == "")
            {
                btnUpdateInformation.Enabled = false;
                btnDeleteInformation.Enabled = false;
                btnAddPractice.Enabled = false;
            }
        }

        private void btnSearchHanViet_Click(object sender, EventArgs e)
        {
            txtMean.Text = string.Empty;
            type = 1;
            string filePath = rootDirectory + @"\Dictionary\Han_Viet.txt";
            //string filePath = rootDirectory + @"\Dictionary\Han_Viet.txt";
            //string filePath = rootDirectory + @"\Dictionary\TestGhi.txt";
            string word = txtSearch.Text;
            bool found = false;
            try
            {
                if (word == "") throw new Exception();
                // Khởi tạo một đối tượng StreamReader để đọc từ file
                using (StreamReader sr = new StreamReader(filePath))
                {
                    string line;
                    // Đọc từng dòng từ file cho đến khi kết thúc file
                    while ((line = sr.ReadLine()) != null)
                    {
                        string[] lineWord = line.Split('=');
                        if (lineWord[0] != "" && lineWord[1].Equals(word))
                        {
                            txtMean.Text += lineWord[0] + Environment.NewLine;
                            //MessageBox.Show(lineWord[1].Replace("\\n\\t", Environment.NewLine));
                            found = true;
                            break;
                        }
                    }
                    if (!found) txtMean.Text = "Không tìm thấy từ này";
                    SetOpen();
                }
            }
            catch (Exception ex)
            {
                // Xử lý các ngoại lệ nếu có
                Console.WriteLine(ex.Message);
            }
        }

        private void btnAddPractice_Click(object sender, EventArgs e)
        {
            try
            {
                var practice = new Practice { word = txtSearch.Text, mean = txtMean.Text, dificult = txtSearch.Text.Length };
                // Thực hiện thêm mới phần tử vào cơ sở dữ liệu
                Practice.InsertWord(practice);
                MessageBox.Show("Add Complete", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        static String GetConnectionString()
        {
            IConfiguration config = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", true, true).Build();
            var strConnection = config.GetConnectionString("MyDB");
            return strConnection;
        }
        
    }
}