﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dictionary
{
    public partial class Form3_3_3 : Form
    {
        int examId;
        string type;
        public Form3_3_3()
        {
            InitializeComponent();
        }
        public Form3_3_3(string t, int e)
        {
            type = t;
            examId = e;
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Form3_3 form3_3 = new Form3_3(type);
            form3_3.FormClosed += Form3_3_FormClosed; //Đóng cái frmPractice lại nè
            form3_3.Show(); //Hiển thị from Practice
            this.Hide(); // Ẩn form FrmSearchWord khi mở form FrmPractice
        }

        private void Form3_3_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close(); // Đóng form FrmSearchWord khi form FrmPractice được đóng
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (type.Equals("HSK1"))
            {
                Form3_3_3_1 form3_3 = new Form3_3_3_1(examId);
                form3_3.FormClosed += Form3_3_3_FormClosed;
                form3_3.Show(); //Hiển thị from Practice
                this.Hide(); // Ẩn form FrmSearchWord khi mở form FrmPractice
            }
            else if (type.Equals("HSK2"))
            {
                Form3_3_3_2 form3_3 = new Form3_3_3_2(examId);
                form3_3.FormClosed += Form3_3_3_FormClosed;
                form3_3.Show(); //Hiển thị from Practice
                this.Hide();
            }
            else if (type.Equals("HSK3"))
            {
                Form3_3_3_3 form3_3 = new Form3_3_3_3(examId);
                form3_3.FormClosed += Form3_3_3_FormClosed;
                form3_3.Show(); //Hiển thị from Practice
                this.Hide();
            }
            else
            {
                Form3_3_3_4_6 form3_3 = new Form3_3_3_4_6(examId, type);
                form3_3.FormClosed += Form3_3_3_FormClosed;
                form3_3.Show(); //Hiển thị from Practice
                this.Hide();
            }
        }
        private void Form3_3_3_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close(); // Đóng form FrmSearchWord khi form FrmPractice được đóng
        }

        private void Form3_3_3_Load(object sender, EventArgs e)
        {
            string tail = "";
            if (type.Equals("HSK1")) tail = "/40";
            else if (type.Equals("HSK2")) tail = "/60";
            else if (type.Equals("HSK3")) tail = "/80";
            else tail = "/100";
            Result result = new Result();
            List<Result> results = new List<Result>();
            results = result.FindAllResult(examId);
            var re = results.Select(x => new
            {
                ResultId = x.ResultId,
                Score = x.Score.ToString() + tail,
                DateTaken = x.DateTaken,
            }).ToList();
            dataGridView1.DataSource = re;
        }

        private void btnSort_Click(object sender, EventArgs e)
        {
            string tail = "";
            if (type.Equals("HSK1")) tail = "/40";
            else if (type.Equals("HSK2")) tail = "/60";
            else if (type.Equals("HSK3")) tail = "/80";
            else tail = "/100";
            Result result = new Result();
            List<Result> results = new List<Result>();
            results = result.FindAllResult(examId);
            var re = results.Select(x => new
            {
                ResultId = x.ResultId,
                Score = x.Score.ToString() + tail,
                DateTaken = x.DateTaken,
            }).OrderByDescending(x=> x.Score).ToList();
            dataGridView1.DataSource = re;
        }
    }
}
