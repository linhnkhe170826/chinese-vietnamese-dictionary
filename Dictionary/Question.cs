﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dictionary
{
    internal class Question
    {
        public int QuestionId { get; set; }
        public int ExamId { get; set; }
        public string QuestionText { get; set; }
        public string CorrectAnswer {  get; set; }

        public Question(int questionId, int examId, string questionText, string correctAnswer)
        {
            QuestionId = questionId;
            ExamId = examId;
            QuestionText = questionText;
            CorrectAnswer = correctAnswer;
        }

        public Question()
        {
        }
        SqlConnection connection;
        SqlCommand command;
        static String GetConnectionString()
        {
            IConfiguration config = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", true, true).Build();
            var strConnection = config.GetConnectionString("MyDB");
            return strConnection;
        }
        public List<Question> GetQuestion(int examId)
        {
            List<Question> questions = new List<Question>();
            String ConnectionString = GetConnectionString();
            SqlConnection connection = new SqlConnection(ConnectionString);
            string Query = "  Select * from Questions where ExamID = @ExamID";
            SqlCommand command = new SqlCommand(Query, connection);
            command.Parameters.AddWithValue("@ExamID", examId);
            try
            {
                connection.Open();
                SqlCommand sqlCommand = new SqlCommand(Query, connection);
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        questions.Add(new Question
                        {
                            QuestionId = reader.GetInt32("QuestionID"),
                            ExamId = reader.GetInt32("ExamID"),
                            QuestionText = reader.GetString("QuestionText"),
                            CorrectAnswer = reader.GetString("CorrectAnswer")
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally { connection.Close(); }
            return questions;
        }
        public int GetStartQuestionId(int examId)
        {
            int questionId = 0;
            String ConnectionString = GetConnectionString();
            SqlConnection connection = new SqlConnection(ConnectionString);
            string Query = " Select Top 1 QuestionID  from Questions where ExamID = @ExamID";
            SqlCommand command = new SqlCommand(Query, connection);
            command.Parameters.AddWithValue("@ExamID", examId);
            try
            {
                connection.Open();
                SqlCommand sqlCommand = new SqlCommand(Query, connection);
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        questionId = reader.GetInt32("QuestionID");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally { connection.Close(); }
            return questionId;
        }
    }
}
