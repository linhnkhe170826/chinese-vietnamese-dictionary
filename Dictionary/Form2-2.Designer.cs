﻿namespace Dictionary
{
    partial class Form2_2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2_2));
            label1 = new Label();
            pictureBox1 = new PictureBox();
            txtWord = new TextBox();
            btnSub = new Button();
            label2 = new Label();
            lbScore = new Label();
            groupBox1 = new GroupBox();
            groupBox2 = new GroupBox();
            txtMean = new TextBox();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).BeginInit();
            groupBox1.SuspendLayout();
            groupBox2.SuspendLayout();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 20F, FontStyle.Bold, GraphicsUnit.Point);
            label1.Location = new Point(320, 0);
            label1.Name = "label1";
            label1.Size = new Size(192, 54);
            label1.TabIndex = 2;
            label1.Text = "Question";
            // 
            // pictureBox1
            // 
            pictureBox1.Image = (Image)resources.GetObject("pictureBox1.Image");
            pictureBox1.Location = new Point(8, 54);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.Size = new Size(214, 181);
            pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox1.TabIndex = 6;
            pictureBox1.TabStop = false;
            // 
            // txtWord
            // 
            txtWord.Font = new Font("Segoe UI", 14F, FontStyle.Regular, GraphicsUnit.Point);
            txtWord.Location = new Point(346, 234);
            txtWord.Name = "txtWord";
            txtWord.Size = new Size(150, 45);
            txtWord.TabIndex = 7;
            // 
            // btnSub
            // 
            btnSub.Location = new Point(362, 295);
            btnSub.Name = "btnSub";
            btnSub.Size = new Size(112, 34);
            btnSub.TabIndex = 8;
            btnSub.Text = "Submit";
            btnSub.UseVisualStyleBackColor = true;
            btnSub.Click += btnSub_Click;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Segoe UI", 14F, FontStyle.Bold, GraphicsUnit.Point);
            label2.Location = new Point(35, 27);
            label2.Name = "label2";
            label2.Size = new Size(97, 38);
            label2.TabIndex = 9;
            label2.Text = "Score:";
            // 
            // lbScore
            // 
            lbScore.AutoSize = true;
            lbScore.Font = new Font("Segoe UI", 14F, FontStyle.Regular, GraphicsUnit.Point);
            lbScore.Location = new Point(68, 75);
            lbScore.Name = "lbScore";
            lbScore.Size = new Size(32, 38);
            lbScore.TabIndex = 10;
            lbScore.Text = "0";
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(lbScore);
            groupBox1.Controls.Add(label2);
            groupBox1.Location = new Point(634, 67);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new Size(166, 150);
            groupBox1.TabIndex = 11;
            groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            groupBox2.Controls.Add(txtMean);
            groupBox2.Controls.Add(groupBox1);
            groupBox2.Controls.Add(btnSub);
            groupBox2.Controls.Add(label1);
            groupBox2.Controls.Add(txtWord);
            groupBox2.Controls.Add(pictureBox1);
            groupBox2.Location = new Point(4, 9);
            groupBox2.Name = "groupBox2";
            groupBox2.Size = new Size(806, 349);
            groupBox2.TabIndex = 12;
            groupBox2.TabStop = false;
            // 
            // txtMean
            // 
            txtMean.Font = new Font("Segoe UI", 10F, FontStyle.Regular, GraphicsUnit.Point);
            txtMean.Location = new Point(247, 67);
            txtMean.Multiline = true;
            txtMean.Name = "txtMean";
            txtMean.ReadOnly = true;
            txtMean.ScrollBars = ScrollBars.Both;
            txtMean.Size = new Size(368, 150);
            txtMean.TabIndex = 12;
            // 
            // Form2_2
            // 
            AutoScaleDimensions = new SizeF(10F, 25F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(816, 370);
            Controls.Add(groupBox2);
            Name = "Form2_2";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Form2_2";
            Load += Form2_2_Load;
            ((System.ComponentModel.ISupportInitialize)pictureBox1).EndInit();
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            groupBox2.ResumeLayout(false);
            groupBox2.PerformLayout();
            ResumeLayout(false);
        }

        #endregion
        private Label label1;
        private PictureBox pictureBox1;
        private TextBox txtWord;
        private Button btnSub;
        private Label label2;
        private Label lbScore;
        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private TextBox txtMean;
    }
}