﻿namespace Dictionary
{
    partial class FrmTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            groupBox3 = new GroupBox();
            lbTest = new Label();
            lbPractice = new Label();
            lbSearchWord = new Label();
            label1 = new Label();
            label5 = new Label();
            label6 = new Label();
            btnHSK1 = new Button();
            groupBox1 = new GroupBox();
            btnClose = new Button();
            btnHSK6 = new Button();
            btnHSK5 = new Button();
            btnHSK4 = new Button();
            btnHSK3 = new Button();
            btnHSK2 = new Button();
            groupBox3.SuspendLayout();
            groupBox1.SuspendLayout();
            SuspendLayout();
            // 
            // groupBox3
            // 
            groupBox3.Controls.Add(lbTest);
            groupBox3.Controls.Add(lbPractice);
            groupBox3.Controls.Add(lbSearchWord);
            groupBox3.Controls.Add(label1);
            groupBox3.Controls.Add(label5);
            groupBox3.Controls.Add(label6);
            groupBox3.Location = new Point(2, 2);
            groupBox3.Name = "groupBox3";
            groupBox3.Size = new Size(573, 125);
            groupBox3.TabIndex = 21;
            groupBox3.TabStop = false;
            // 
            // lbTest
            // 
            lbTest.AutoSize = true;
            lbTest.Font = new Font("Segoe UI", 11F, FontStyle.Underline, GraphicsUnit.Point);
            lbTest.Location = new Point(250, 82);
            lbTest.Name = "lbTest";
            lbTest.Size = new Size(101, 30);
            lbTest.TabIndex = 21;
            lbTest.Text = "Luyện đề";
            lbTest.Click += lbTest_Click;
            // 
            // lbPractice
            // 
            lbPractice.AutoSize = true;
            lbPractice.Font = new Font("Segoe UI", 11F, FontStyle.Underline, GraphicsUnit.Point);
            lbPractice.Location = new Point(140, 83);
            lbPractice.Name = "lbPractice";
            lbPractice.Size = new Size(79, 30);
            lbPractice.TabIndex = 20;
            lbPractice.Text = "Ôn tập";
            lbPractice.Click += lbPractice_Click;
            // 
            // lbSearchWord
            // 
            lbSearchWord.AutoSize = true;
            lbSearchWord.Font = new Font("Segoe UI", 11F, FontStyle.Underline, GraphicsUnit.Point);
            lbSearchWord.Location = new Point(43, 83);
            lbSearchWord.Name = "lbSearchWord";
            lbSearchWord.Size = new Size(68, 30);
            lbSearchWord.TabIndex = 19;
            lbSearchWord.Text = "Tra từ";
            lbSearchWord.Click += lbSearchWord_Click;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI Black", 25F, FontStyle.Bold, GraphicsUnit.Point);
            label1.Location = new Point(23, 16);
            label1.Name = "label1";
            label1.Size = new Size(492, 67);
            label1.TabIndex = 18;
            label1.Text = "Từ điển Trung Việt";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new Font("Segoe UI", 30F, FontStyle.Bold, GraphicsUnit.Point);
            label5.Location = new Point(102, 41);
            label5.Name = "label5";
            label5.Size = new Size(51, 81);
            label5.TabIndex = 22;
            label5.Text = ".";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new Font("Segoe UI", 30F, FontStyle.Bold, GraphicsUnit.Point);
            label6.Location = new Point(207, 42);
            label6.Name = "label6";
            label6.Size = new Size(51, 81);
            label6.TabIndex = 23;
            label6.Text = ".";
            // 
            // btnHSK1
            // 
            btnHSK1.Font = new Font("Segoe UI", 20F, FontStyle.Bold, GraphicsUnit.Point);
            btnHSK1.Location = new Point(54, 19);
            btnHSK1.Name = "btnHSK1";
            btnHSK1.Size = new Size(461, 61);
            btnHSK1.TabIndex = 22;
            btnHSK1.Text = "HSK1";
            btnHSK1.UseVisualStyleBackColor = true;
            btnHSK1.Click += btnHSK1_Click;
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(btnClose);
            groupBox1.Controls.Add(btnHSK6);
            groupBox1.Controls.Add(btnHSK5);
            groupBox1.Controls.Add(btnHSK4);
            groupBox1.Controls.Add(btnHSK3);
            groupBox1.Controls.Add(btnHSK2);
            groupBox1.Controls.Add(btnHSK1);
            groupBox1.Location = new Point(2, 118);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new Size(573, 462);
            groupBox1.TabIndex = 23;
            groupBox1.TabStop = false;
            // 
            // btnClose
            // 
            btnClose.Location = new Point(221, 422);
            btnClose.Name = "btnClose";
            btnClose.Size = new Size(121, 34);
            btnClose.TabIndex = 28;
            btnClose.Text = "Close";
            btnClose.UseVisualStyleBackColor = true;
            btnClose.Click += btnClose_Click;
            // 
            // btnHSK6
            // 
            btnHSK6.Font = new Font("Segoe UI", 20F, FontStyle.Bold, GraphicsUnit.Point);
            btnHSK6.Location = new Point(54, 354);
            btnHSK6.Name = "btnHSK6";
            btnHSK6.Size = new Size(461, 61);
            btnHSK6.TabIndex = 27;
            btnHSK6.Text = "HSK6";
            btnHSK6.UseVisualStyleBackColor = true;
            btnHSK6.Click += btnHSK6_Click;
            // 
            // btnHSK5
            // 
            btnHSK5.Font = new Font("Segoe UI", 20F, FontStyle.Bold, GraphicsUnit.Point);
            btnHSK5.Location = new Point(54, 287);
            btnHSK5.Name = "btnHSK5";
            btnHSK5.Size = new Size(461, 61);
            btnHSK5.TabIndex = 26;
            btnHSK5.Text = "HSK5";
            btnHSK5.UseVisualStyleBackColor = true;
            btnHSK5.Click += btnHSK5_Click;
            // 
            // btnHSK4
            // 
            btnHSK4.Font = new Font("Segoe UI", 20F, FontStyle.Bold, GraphicsUnit.Point);
            btnHSK4.Location = new Point(54, 220);
            btnHSK4.Name = "btnHSK4";
            btnHSK4.Size = new Size(461, 61);
            btnHSK4.TabIndex = 25;
            btnHSK4.Text = "HSK4";
            btnHSK4.UseVisualStyleBackColor = true;
            btnHSK4.Click += btnHSK4_Click;
            // 
            // btnHSK3
            // 
            btnHSK3.Font = new Font("Segoe UI", 20F, FontStyle.Bold, GraphicsUnit.Point);
            btnHSK3.Location = new Point(54, 153);
            btnHSK3.Name = "btnHSK3";
            btnHSK3.Size = new Size(461, 61);
            btnHSK3.TabIndex = 24;
            btnHSK3.Text = "HSK3";
            btnHSK3.UseVisualStyleBackColor = true;
            btnHSK3.Click += btnHSK3_Click;
            // 
            // btnHSK2
            // 
            btnHSK2.Font = new Font("Segoe UI", 20F, FontStyle.Bold, GraphicsUnit.Point);
            btnHSK2.Location = new Point(54, 86);
            btnHSK2.Name = "btnHSK2";
            btnHSK2.Size = new Size(461, 61);
            btnHSK2.TabIndex = 23;
            btnHSK2.Text = "HSK2";
            btnHSK2.UseVisualStyleBackColor = true;
            btnHSK2.Click += btnHSK2_Click;
            // 
            // FrmTest
            // 
            AutoScaleDimensions = new SizeF(10F, 25F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(586, 585);
            Controls.Add(groupBox3);
            Controls.Add(groupBox1);
            Name = "FrmTest";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Luyện đề";
            Load += frmTest_Load;
            groupBox3.ResumeLayout(false);
            groupBox3.PerformLayout();
            groupBox1.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private GroupBox groupBox3;
        private Label lbTest;
        private Label lbPractice;
        private Label lbSearchWord;
        private Label label1;
        private Label label5;
        private Label label6;
        private Button btnHSK1;
        private GroupBox groupBox1;
        private Button btnHSK6;
        private Button btnHSK5;
        private Button btnHSK4;
        private Button btnHSK3;
        private Button btnHSK2;
        private Button btnClose;
    }
}