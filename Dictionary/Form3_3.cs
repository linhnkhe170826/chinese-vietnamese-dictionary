﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dictionary
{
    public partial class Form3_3 : Form
    {
        String type;
        Exams exams = new Exams();
        public Form3_3()
        {
            InitializeComponent();
        }
        public Form3_3(string t)
        {
            type = t;
            InitializeComponent();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            FrmTest frmTest = new FrmTest();
            frmTest.FormClosed += FrmTest_FormClosed;
            frmTest.Show();
            this.Hide(); // Ẩn form FrmSearchWord khi mở form FrmTest
        }
        private void FrmTest_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close(); // Đóng form FrmSearchWord khi form FrmTest được đóng
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form3_3_Load(object sender, EventArgs e)
        {
            var li = exams.GetExams(type).Select(x => new
            {
                ExamsId = x.ExamsId,
                Title = x.Title,
                Type = x.Type,
                Date = x.Date,
            }).ToList();
            dataGridView1.DataSource = li;
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int ExamsId = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells["ExamsId"].Value);
            Form3_3_3 form3_3_3 = new Form3_3_3(type, ExamsId);
            form3_3_3.FormClosed += Form3_3_3_FormClosed;
            form3_3_3.Show();
            this.Hide(); // Ẩn form FrmSearchWord khi mở form FrmTest
        }
        private void Form3_3_3_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close(); // Đóng form FrmSearchWord khi form FrmTest được đóng
        }
    }
}
