﻿namespace Dictionary
{
    partial class FrmPractice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            groupBox3 = new GroupBox();
            lbTest = new Label();
            lbPractice = new Label();
            lbSearchWord = new Label();
            label1 = new Label();
            label5 = new Label();
            label6 = new Label();
            dataGridView1 = new DataGridView();
            btnPractice = new Button();
            btnClose = new Button();
            groupBox1 = new GroupBox();
            label2 = new Label();
            Level = new Label();
            txtSearch = new TextBox();
            btnDelete = new Button();
            txtMean = new TextBox();
            txtWord = new TextBox();
            cbDifficult = new ComboBox();
            cbAlphabet = new ComboBox();
            groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dataGridView1).BeginInit();
            groupBox1.SuspendLayout();
            SuspendLayout();
            // 
            // groupBox3
            // 
            groupBox3.Controls.Add(lbTest);
            groupBox3.Controls.Add(lbPractice);
            groupBox3.Controls.Add(lbSearchWord);
            groupBox3.Controls.Add(label1);
            groupBox3.Controls.Add(label5);
            groupBox3.Controls.Add(label6);
            groupBox3.Location = new Point(7, 3);
            groupBox3.Name = "groupBox3";
            groupBox3.Size = new Size(996, 125);
            groupBox3.TabIndex = 21;
            groupBox3.TabStop = false;
            // 
            // lbTest
            // 
            lbTest.AutoSize = true;
            lbTest.Font = new Font("Segoe UI", 11F, FontStyle.Underline, GraphicsUnit.Point);
            lbTest.Location = new Point(250, 82);
            lbTest.Name = "lbTest";
            lbTest.Size = new Size(101, 30);
            lbTest.TabIndex = 21;
            lbTest.Text = "Luyện đề";
            lbTest.Click += lbTest_Click;
            // 
            // lbPractice
            // 
            lbPractice.AutoSize = true;
            lbPractice.Font = new Font("Segoe UI", 11F, FontStyle.Underline, GraphicsUnit.Point);
            lbPractice.Location = new Point(140, 83);
            lbPractice.Name = "lbPractice";
            lbPractice.Size = new Size(79, 30);
            lbPractice.TabIndex = 20;
            lbPractice.Text = "Ôn tập";
            lbPractice.Click += lbPractice_Click;
            // 
            // lbSearchWord
            // 
            lbSearchWord.AutoSize = true;
            lbSearchWord.Font = new Font("Segoe UI", 11F, FontStyle.Underline, GraphicsUnit.Point);
            lbSearchWord.Location = new Point(43, 83);
            lbSearchWord.Name = "lbSearchWord";
            lbSearchWord.Size = new Size(68, 30);
            lbSearchWord.TabIndex = 19;
            lbSearchWord.Text = "Tra từ";
            lbSearchWord.Click += lbSearchWord_Click;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI Black", 25F, FontStyle.Bold, GraphicsUnit.Point);
            label1.Location = new Point(23, 16);
            label1.Name = "label1";
            label1.Size = new Size(492, 67);
            label1.TabIndex = 18;
            label1.Text = "Từ điển Trung Việt";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new Font("Segoe UI", 30F, FontStyle.Bold, GraphicsUnit.Point);
            label5.Location = new Point(102, 41);
            label5.Name = "label5";
            label5.Size = new Size(51, 81);
            label5.TabIndex = 22;
            label5.Text = ".";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new Font("Segoe UI", 30F, FontStyle.Bold, GraphicsUnit.Point);
            label6.Location = new Point(207, 42);
            label6.Name = "label6";
            label6.Size = new Size(51, 81);
            label6.TabIndex = 23;
            label6.Text = ".";
            // 
            // dataGridView1
            // 
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridView1.Location = new Point(5, 70);
            dataGridView1.Name = "dataGridView1";
            dataGridView1.ReadOnly = true;
            dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            dataGridView1.RowTemplate.Height = 33;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.Size = new Size(550, 276);
            dataGridView1.TabIndex = 22;
            dataGridView1.CellMouseClick += dataGridView1_CellMouseClick;
            // 
            // btnPractice
            // 
            btnPractice.Font = new Font("Segoe UI", 14F, FontStyle.Regular, GraphicsUnit.Point);
            btnPractice.Location = new Point(847, 70);
            btnPractice.Name = "btnPractice";
            btnPractice.Size = new Size(127, 96);
            btnPractice.TabIndex = 23;
            btnPractice.Text = "Start practice";
            btnPractice.UseVisualStyleBackColor = true;
            btnPractice.Click += btnPractice_Click;
            // 
            // btnClose
            // 
            btnClose.Location = new Point(847, 291);
            btnClose.Name = "btnClose";
            btnClose.Size = new Size(127, 34);
            btnClose.TabIndex = 29;
            btnClose.Text = "Close";
            btnClose.UseVisualStyleBackColor = true;
            btnClose.Click += btnClose_Click;
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(label2);
            groupBox1.Controls.Add(Level);
            groupBox1.Controls.Add(txtSearch);
            groupBox1.Controls.Add(btnDelete);
            groupBox1.Controls.Add(txtMean);
            groupBox1.Controls.Add(txtWord);
            groupBox1.Controls.Add(cbDifficult);
            groupBox1.Controls.Add(cbAlphabet);
            groupBox1.Controls.Add(dataGridView1);
            groupBox1.Controls.Add(btnClose);
            groupBox1.Controls.Add(btnPractice);
            groupBox1.Location = new Point(7, 119);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new Size(996, 352);
            groupBox1.TabIndex = 30;
            groupBox1.TabStop = false;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(789, 33);
            label2.Name = "label2";
            label2.Size = new Size(75, 25);
            label2.TabIndex = 37;
            label2.Text = "Sắp xếp";
            // 
            // Level
            // 
            Level.AutoSize = true;
            Level.Location = new Point(573, 31);
            Level.Name = "Level";
            Level.Size = new Size(65, 25);
            Level.TabIndex = 36;
            Level.Text = "Độ dài";
            // 
            // txtSearch
            // 
            txtSearch.Location = new Point(6, 30);
            txtSearch.Name = "txtSearch";
            txtSearch.Size = new Size(549, 31);
            txtSearch.TabIndex = 35;
            txtSearch.TextChanged += txtSearch_TextChanged;
            // 
            // btnDelete
            // 
            btnDelete.Font = new Font("Segoe UI", 14F, FontStyle.Regular, GraphicsUnit.Point);
            btnDelete.Location = new Point(847, 195);
            btnDelete.Name = "btnDelete";
            btnDelete.Size = new Size(127, 70);
            btnDelete.TabIndex = 34;
            btnDelete.Text = "Delete";
            btnDelete.UseVisualStyleBackColor = true;
            btnDelete.Click += btnDelete_Click;
            // 
            // txtMean
            // 
            txtMean.Location = new Point(573, 107);
            txtMean.Multiline = true;
            txtMean.Name = "txtMean";
            txtMean.ReadOnly = true;
            txtMean.ScrollBars = ScrollBars.Vertical;
            txtMean.Size = new Size(252, 239);
            txtMean.TabIndex = 33;
            // 
            // txtWord
            // 
            txtWord.Location = new Point(573, 70);
            txtWord.Name = "txtWord";
            txtWord.ReadOnly = true;
            txtWord.Size = new Size(252, 31);
            txtWord.TabIndex = 32;
            // 
            // cbDifficult
            // 
            cbDifficult.FormattingEnabled = true;
            cbDifficult.Location = new Point(644, 27);
            cbDifficult.Name = "cbDifficult";
            cbDifficult.Size = new Size(127, 33);
            cbDifficult.TabIndex = 31;
            cbDifficult.SelectedIndexChanged += cbDifficult_SelectedIndexChanged;
            // 
            // cbAlphabet
            // 
            cbAlphabet.FormattingEnabled = true;
            cbAlphabet.Location = new Point(870, 28);
            cbAlphabet.Name = "cbAlphabet";
            cbAlphabet.Size = new Size(104, 33);
            cbAlphabet.TabIndex = 30;
            cbAlphabet.SelectedIndexChanged += cbAlphabet_SelectedIndexChanged;
            // 
            // FrmPractice
            // 
            AutoScaleDimensions = new SizeF(10F, 25F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1011, 489);
            Controls.Add(groupBox3);
            Controls.Add(groupBox1);
            Name = "FrmPractice";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Ôn tập";
            Load += FrmPractice_Load;
            groupBox3.ResumeLayout(false);
            groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)dataGridView1).EndInit();
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private GroupBox groupBox3;
        private Label lbTest;
        private Label lbPractice;
        private Label lbSearchWord;
        private Label label1;
        private Label label5;
        private Label label6;
        private DataGridView dataGridView1;
        private Button btnPractice;
        private Button btnClose;
        private GroupBox groupBox1;
        private TextBox txtMean;
        private TextBox txtWord;
        private ComboBox cbDifficult;
        private ComboBox cbAlphabet;
        private Button btnDelete;
        private TextBox txtSearch;
        private Label Level;
        private Label label2;
    }
}