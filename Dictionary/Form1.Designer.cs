﻿namespace Dictionary
{
    partial class FrmSearchWord
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            btnSearchOnWeb = new Button();
            btnSearchPinyin = new Button();
            btnSearchHanViet = new Button();
            btnUpdateInformation = new Button();
            btnAddPractice = new Button();
            btnDeleteInformation = new Button();
            btnClear = new Button();
            btnClose = new Button();
            txtSearch = new TextBox();
            groupBox1 = new GroupBox();
            groupBox2 = new GroupBox();
            txtMean = new TextBox();
            label1 = new Label();
            groupBox3 = new GroupBox();
            lbTest = new Label();
            lbPractice = new Label();
            lbSearchWord = new Label();
            label5 = new Label();
            label6 = new Label();
            groupBox4 = new GroupBox();
            groupBox1.SuspendLayout();
            groupBox2.SuspendLayout();
            groupBox3.SuspendLayout();
            groupBox4.SuspendLayout();
            SuspendLayout();
            // 
            // btnSearchOnWeb
            // 
            btnSearchOnWeb.Location = new Point(740, 30);
            btnSearchOnWeb.Name = "btnSearchOnWeb";
            btnSearchOnWeb.Size = new Size(135, 61);
            btnSearchOnWeb.TabIndex = 4;
            btnSearchOnWeb.Text = "Tìm kiếm trên web";
            btnSearchOnWeb.UseVisualStyleBackColor = true;
            // 
            // btnSearchPinyin
            // 
            btnSearchPinyin.Location = new Point(107, 97);
            btnSearchPinyin.Name = "btnSearchPinyin";
            btnSearchPinyin.Size = new Size(250, 43);
            btnSearchPinyin.TabIndex = 6;
            btnSearchPinyin.Text = "Tra pinyin";
            btnSearchPinyin.UseVisualStyleBackColor = true;
            btnSearchPinyin.Click += btnSearchPinyin_Click;
            // 
            // btnSearchHanViet
            // 
            btnSearchHanViet.Location = new Point(518, 97);
            btnSearchHanViet.Name = "btnSearchHanViet";
            btnSearchHanViet.Size = new Size(250, 43);
            btnSearchHanViet.TabIndex = 7;
            btnSearchHanViet.Text = "Tra âm hán việt";
            btnSearchHanViet.UseVisualStyleBackColor = true;
            btnSearchHanViet.Click += btnSearchHanViet_Click;
            // 
            // btnUpdateInformation
            // 
            btnUpdateInformation.Location = new Point(767, 89);
            btnUpdateInformation.Name = "btnUpdateInformation";
            btnUpdateInformation.Size = new Size(121, 64);
            btnUpdateInformation.TabIndex = 9;
            btnUpdateInformation.Text = "Cập nhật thông tin";
            btnUpdateInformation.UseVisualStyleBackColor = true;
            btnUpdateInformation.Click += btnUpdateInformation_Click;
            // 
            // btnAddPractice
            // 
            btnAddPractice.Location = new Point(767, 249);
            btnAddPractice.Name = "btnAddPractice";
            btnAddPractice.Size = new Size(121, 60);
            btnAddPractice.TabIndex = 10;
            btnAddPractice.Text = "Thêm vào mục ôn tập";
            btnAddPractice.UseVisualStyleBackColor = true;
            btnAddPractice.Click += btnAddPractice_Click;
            // 
            // btnDeleteInformation
            // 
            btnDeleteInformation.Location = new Point(767, 169);
            btnDeleteInformation.Name = "btnDeleteInformation";
            btnDeleteInformation.Size = new Size(121, 65);
            btnDeleteInformation.TabIndex = 11;
            btnDeleteInformation.Text = "Xóa thông tin";
            btnDeleteInformation.UseVisualStyleBackColor = true;
            btnDeleteInformation.Click += btnDeleteInformation_Click;
            // 
            // btnClear
            // 
            btnClear.Location = new Point(767, 39);
            btnClear.Name = "btnClear";
            btnClear.Size = new Size(121, 34);
            btnClear.TabIndex = 12;
            btnClear.Text = "Clear";
            btnClear.UseVisualStyleBackColor = true;
            btnClear.Click += btnClear_Click;
            // 
            // btnClose
            // 
            btnClose.Location = new Point(394, 528);
            btnClose.Name = "btnClose";
            btnClose.Size = new Size(121, 34);
            btnClose.TabIndex = 14;
            btnClose.Text = "Close";
            btnClose.UseVisualStyleBackColor = true;
            btnClose.Click += btnClose_Click;
            // 
            // txtSearch
            // 
            txtSearch.Font = new Font("Segoe UI", 20F, FontStyle.Regular, GraphicsUnit.Point);
            txtSearch.Location = new Point(24, 30);
            txtSearch.Name = "txtSearch";
            txtSearch.Size = new Size(710, 61);
            txtSearch.TabIndex = 0;
            txtSearch.TextChanged += txtSearch_TextChanged;
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(txtSearch);
            groupBox1.Controls.Add(btnSearchOnWeb);
            groupBox1.Controls.Add(btnSearchHanViet);
            groupBox1.Controls.Add(btnSearchPinyin);
            groupBox1.Location = new Point(16, 15);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new Size(910, 146);
            groupBox1.TabIndex = 15;
            groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            groupBox2.Controls.Add(txtMean);
            groupBox2.Controls.Add(btnClear);
            groupBox2.Controls.Add(btnAddPractice);
            groupBox2.Controls.Add(btnDeleteInformation);
            groupBox2.Controls.Add(btnUpdateInformation);
            groupBox2.Location = new Point(16, 167);
            groupBox2.Name = "groupBox2";
            groupBox2.Size = new Size(910, 345);
            groupBox2.TabIndex = 16;
            groupBox2.TabStop = false;
            // 
            // txtMean
            // 
            txtMean.Font = new Font("Segoe UI", 11F, FontStyle.Regular, GraphicsUnit.Point);
            txtMean.Location = new Point(39, 30);
            txtMean.Multiline = true;
            txtMean.Name = "txtMean";
            txtMean.ReadOnly = true;
            txtMean.ScrollBars = ScrollBars.Vertical;
            txtMean.Size = new Size(712, 292);
            txtMean.TabIndex = 13;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI Black", 25F, FontStyle.Bold, GraphicsUnit.Point);
            label1.Location = new Point(23, 16);
            label1.Name = "label1";
            label1.Size = new Size(492, 67);
            label1.TabIndex = 18;
            label1.Text = "Từ điển Trung Việt";
            // 
            // groupBox3
            // 
            groupBox3.Controls.Add(lbTest);
            groupBox3.Controls.Add(lbPractice);
            groupBox3.Controls.Add(lbSearchWord);
            groupBox3.Controls.Add(label1);
            groupBox3.Controls.Add(label5);
            groupBox3.Controls.Add(label6);
            groupBox3.Location = new Point(4, 2);
            groupBox3.Name = "groupBox3";
            groupBox3.Size = new Size(941, 125);
            groupBox3.TabIndex = 19;
            groupBox3.TabStop = false;
            // 
            // lbTest
            // 
            lbTest.AutoSize = true;
            lbTest.Font = new Font("Segoe UI", 11F, FontStyle.Underline, GraphicsUnit.Point);
            lbTest.Location = new Point(250, 82);
            lbTest.Name = "lbTest";
            lbTest.Size = new Size(101, 30);
            lbTest.TabIndex = 21;
            lbTest.Text = "Luyện đề";
            lbTest.Click += lbTest_Click;
            // 
            // lbPractice
            // 
            lbPractice.AutoSize = true;
            lbPractice.Font = new Font("Segoe UI", 11F, FontStyle.Underline, GraphicsUnit.Point);
            lbPractice.Location = new Point(140, 83);
            lbPractice.Name = "lbPractice";
            lbPractice.Size = new Size(79, 30);
            lbPractice.TabIndex = 20;
            lbPractice.Text = "Ôn tập";
            lbPractice.Click += lbPractice_Click;
            // 
            // lbSearchWord
            // 
            lbSearchWord.AutoSize = true;
            lbSearchWord.Font = new Font("Segoe UI", 11F, FontStyle.Underline, GraphicsUnit.Point);
            lbSearchWord.Location = new Point(43, 83);
            lbSearchWord.Name = "lbSearchWord";
            lbSearchWord.Size = new Size(68, 30);
            lbSearchWord.TabIndex = 19;
            lbSearchWord.Text = "Tra từ";
            lbSearchWord.Click += lbSearchWord_Click;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new Font("Segoe UI", 30F, FontStyle.Bold, GraphicsUnit.Point);
            label5.Location = new Point(102, 41);
            label5.Name = "label5";
            label5.Size = new Size(51, 81);
            label5.TabIndex = 22;
            label5.Text = ".";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new Font("Segoe UI", 30F, FontStyle.Bold, GraphicsUnit.Point);
            label6.Location = new Point(207, 42);
            label6.Name = "label6";
            label6.Size = new Size(51, 81);
            label6.TabIndex = 23;
            label6.Text = ".";
            // 
            // groupBox4
            // 
            groupBox4.Controls.Add(btnClose);
            groupBox4.Controls.Add(groupBox1);
            groupBox4.Controls.Add(groupBox2);
            groupBox4.Location = new Point(4, 133);
            groupBox4.Name = "groupBox4";
            groupBox4.Size = new Size(941, 577);
            groupBox4.TabIndex = 20;
            groupBox4.TabStop = false;
            // 
            // FrmSearchWord
            // 
            AutoScaleDimensions = new SizeF(10F, 25F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(950, 722);
            Controls.Add(groupBox3);
            Controls.Add(groupBox4);
            Name = "FrmSearchWord";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Tra từ";
            Load += FrmSearchWord_Load;
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            groupBox2.ResumeLayout(false);
            groupBox2.PerformLayout();
            groupBox3.ResumeLayout(false);
            groupBox3.PerformLayout();
            groupBox4.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion
        private DataGridView dataGridView2;
        private Button btnSearchOnWeb;
        private Button btnSearchPinyin;
        private Button btnSearchHanViet;
        private Button btnUpdateInformation;
        private Button btnAddPractice;
        private Button btnDeleteInformation;
        private Button btnClear;
        private Button btnClose;
        private TextBox txtSearch;
        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private Label label1;
        private GroupBox groupBox3;
        private Label lbTest;
        private Label lbPractice;
        private Label lbSearchWord;
        private GroupBox groupBox4;
        private Label label5;
        private Label label6;
        private TextBox txtMean;
    }
}