﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dictionary
{
    internal class Exams
    {
        public int ExamsId { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public DateTime Date {  get; set; }
        public string Listening { get; set; }
        public Exams(int examsId, string title, string type, DateTime date, string listening)
        {
            ExamsId = examsId;
            Title = title;
            Type = type;
            Date = date;
            Listening = listening;
        }

        public Exams()
        {
        }

        SqlConnection connection;
        SqlCommand command;
        static String GetConnectionString()
        {
            IConfiguration config = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", true, true).Build();
            var strConnection = config.GetConnectionString("MyDB");
            return strConnection;
        }
        public List<Exams> GetExams(string type)
        {
            List<Exams> exams = new List<Exams>();
            String ConnectionString = GetConnectionString();
            SqlConnection connection = new SqlConnection(ConnectionString);
            string Query = "Select * from Exams where Description = @Type";
            SqlCommand command = new SqlCommand(Query, connection);
            command.Parameters.AddWithValue("@Type", type);
            try
            {
                connection.Open();
                SqlCommand sqlCommand = new SqlCommand(Query, connection);
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        exams.Add(new Exams
                        {
                            ExamsId = reader.GetInt32("ExamID"),
                            Title = reader.GetString("Title"),
                            Type = reader.GetString("Description"),
                            Date = reader.GetDateTime("Date"),
                            Listening = reader.GetString("Listening")
                        }) ;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally { connection.Close(); }
            return exams;
        }
        public string GetListen(int ExamId)
        {
            string link = "";
            String ConnectionString = GetConnectionString();
            SqlConnection connection = new SqlConnection(ConnectionString);
            string Query = "Select * from Exams where ExamID = @ExamId";
            SqlCommand command = new SqlCommand(Query, connection);
            command.Parameters.AddWithValue("@ExamId", ExamId);
            try
            {
                connection.Open();
                SqlCommand sqlCommand = new SqlCommand(Query, connection);
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        link = reader.GetString("Listening");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally { connection.Close(); }
            return link;
        }
    }
}
