USE [master]
GO
/****** Object:  Database [Dictionary]    Script Date: 3/19/2024 2:07:21 PM ******/
CREATE DATABASE [Dictionary]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Dictionary', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.NKLINH\MSSQL\DATA\Dictionary.mdf' , SIZE = 73728KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Dictionary_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.NKLINH\MSSQL\DATA\Dictionary_log.ldf' , SIZE = 139264KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [Dictionary] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Dictionary].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Dictionary] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Dictionary] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Dictionary] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Dictionary] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Dictionary] SET ARITHABORT OFF 
GO
ALTER DATABASE [Dictionary] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Dictionary] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Dictionary] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Dictionary] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Dictionary] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Dictionary] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Dictionary] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Dictionary] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Dictionary] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Dictionary] SET  ENABLE_BROKER 
GO
ALTER DATABASE [Dictionary] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Dictionary] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Dictionary] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Dictionary] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Dictionary] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Dictionary] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Dictionary] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Dictionary] SET RECOVERY FULL 
GO
ALTER DATABASE [Dictionary] SET  MULTI_USER 
GO
ALTER DATABASE [Dictionary] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Dictionary] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Dictionary] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Dictionary] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Dictionary] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Dictionary] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'Dictionary', N'ON'
GO
ALTER DATABASE [Dictionary] SET QUERY_STORE = OFF
GO
USE [Dictionary]
GO
/****** Object:  Table [dbo].[Answers]    Script Date: 3/19/2024 2:07:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Answers](
	[AnswerID] [int] IDENTITY(1,1) NOT NULL,
	[QuestionID] [int] NULL,
	[AnswerText] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[AnswerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Exams]    Script Date: 3/19/2024 2:07:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Exams](
	[ExamID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](100) NULL,
	[Description] [nvarchar](255) NULL,
	[Date] [date] NULL,
	[Listening] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[ExamID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Practice]    Script Date: 3/19/2024 2:07:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Practice](
	[Word] [nvarchar](50) NULL,
	[Mean] [nvarchar](2555) NULL,
	[Dificult] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Questions]    Script Date: 3/19/2024 2:07:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Questions](
	[QuestionID] [int] IDENTITY(1,1) NOT NULL,
	[ExamID] [int] NULL,
	[QuestionText] [nvarchar](500) NULL,
	[CorrectAnswer] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[QuestionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Results]    Script Date: 3/19/2024 2:07:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Results](
	[ResultID] [int] IDENTITY(1,1) NOT NULL,
	[ExamID] [int] NULL,
	[Score] [int] NULL,
	[DateTaken] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ResultID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StudentResponses]    Script Date: 3/19/2024 2:07:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudentResponses](
	[ResponseID] [int] IDENTITY(1,1) NOT NULL,
	[ResultID] [int] NULL,
	[QuestionID] [int] NULL,
	[SelectedAnswer] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[ResponseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Answers] ON 

INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (353, 1, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (354, 1, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (355, 2, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (356, 2, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (357, 3, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (358, 3, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (359, 4, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (360, 4, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (361, 5, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (362, 5, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (363, 6, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (364, 6, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (365, 6, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (366, 7, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (367, 7, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (368, 7, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (369, 8, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (370, 8, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (371, 8, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (372, 9, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (373, 9, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (374, 9, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (375, 10, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (376, 10, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (377, 10, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (378, 11, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (379, 11, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (380, 11, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (381, 11, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (382, 11, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (383, 11, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (384, 12, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (385, 12, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (386, 12, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (387, 12, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (388, 12, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (389, 12, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (390, 13, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (391, 13, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (392, 13, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (393, 13, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (394, 13, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (395, 13, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (396, 14, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (397, 14, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (398, 14, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (399, 14, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (400, 14, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (401, 14, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (402, 15, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (403, 15, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (404, 15, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (405, 15, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (406, 15, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (407, 15, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (408, 16, N'他的')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (409, 16, N'我的')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (410, 16, N'同学的')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (411, 17, N'星期三')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (412, 17, N'星期五')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (413, 17, N'星期六')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (414, 18, N'5')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (415, 18, N'15')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (416, 18, N'50')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (417, 19, N'茶')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (418, 19, N'苹果')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (419, 19, N'杯子')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (420, 20, N'爱学习')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (421, 20, N'很漂亮')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (422, 20, N'想回家')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (423, 21, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (424, 21, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (425, 22, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (426, 22, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (427, 23, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (428, 23, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (429, 24, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (430, 24, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (431, 25, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (432, 25, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (433, 26, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (434, 26, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (435, 26, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (436, 26, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (437, 26, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (438, 26, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (439, 27, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (440, 27, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (441, 27, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (442, 27, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (443, 27, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (444, 27, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (445, 28, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (446, 28, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (447, 28, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (448, 28, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (449, 28, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (450, 28, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (451, 29, N'A')
GO
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (452, 29, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (453, 29, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (454, 29, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (455, 29, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (456, 29, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (457, 30, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (458, 30, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (459, 30, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (460, 30, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (461, 30, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (462, 30, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (463, 31, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (464, 31, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (465, 31, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (466, 31, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (467, 31, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (468, 31, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (469, 32, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (470, 32, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (471, 32, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (472, 32, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (473, 32, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (474, 32, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (475, 33, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (476, 33, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (477, 33, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (478, 33, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (479, 33, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (480, 33, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (487, 34, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (488, 34, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (489, 34, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (490, 34, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (491, 34, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (492, 34, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (493, 35, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (494, 35, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (495, 35, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (496, 35, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (497, 35, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (498, 35, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (499, 36, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (500, 36, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (501, 36, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (502, 36, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (503, 36, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (504, 36, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (505, 37, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (506, 37, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (507, 37, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (508, 37, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (509, 37, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (510, 37, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (511, 38, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (512, 38, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (513, 38, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (514, 38, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (515, 38, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (516, 38, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (517, 39, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (518, 39, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (519, 39, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (520, 39, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (521, 39, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (522, 39, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (523, 40, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (524, 40, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (525, 40, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (526, 40, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (527, 40, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (528, 40, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (529, 41, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (530, 41, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (531, 42, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (532, 42, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (533, 43, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (534, 43, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (535, 44, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (536, 44, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (537, 45, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (538, 45, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (539, 46, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (540, 46, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (541, 47, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (542, 47, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (543, 48, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (544, 48, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (545, 49, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (546, 49, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (547, 50, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (548, 50, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (549, 51, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (550, 51, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (551, 51, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (552, 51, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (553, 51, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (554, 51, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (555, 52, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (556, 52, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (557, 52, N'C')
GO
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (558, 52, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (559, 52, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (560, 52, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (561, 53, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (562, 53, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (563, 53, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (564, 53, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (565, 53, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (566, 53, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (567, 54, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (568, 54, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (569, 54, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (570, 54, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (571, 54, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (572, 54, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (573, 55, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (574, 55, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (575, 55, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (576, 55, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (577, 55, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (578, 55, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (579, 56, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (580, 56, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (581, 56, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (582, 56, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (583, 56, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (584, 57, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (585, 57, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (586, 57, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (587, 57, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (588, 57, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (589, 58, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (590, 58, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (591, 58, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (592, 58, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (593, 58, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (594, 59, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (595, 59, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (596, 59, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (597, 59, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (598, 59, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (599, 60, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (600, 60, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (601, 60, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (602, 60, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (603, 60, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (604, 61, N'工作')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (605, 61, N'打篮球')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (606, 61, N'看电视')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (607, 62, N'饭店')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (608, 62, N'商店')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (609, 62, N'教师')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (610, 63, N'200多')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (611, 63, N'2000多')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (612, 63, N'3000多')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (613, 64, N'2007年')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (614, 64, N'2008年')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (615, 64, N'2009年')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (616, 65, N'太累了')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (617, 65, N'看过了')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (618, 65, N'有别的事')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (619, 66, N'鱼')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (620, 66, N'西瓜')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (621, 66, N'鸡蛋')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (622, 67, N'机场')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (623, 67, N'医院')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (624, 67, N'火车站')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (625, 68, N'很便宜')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (626, 68, N'颜色好')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (627, 68, N'有点儿高')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (628, 69, N'7月号')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (629, 69, N'8月7号')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (630, 69, N'8月17号')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (631, 70, N'爸爸')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (632, 70, N'妈妈')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (633, 70, N'送牛奶的')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (634, 71, N'吃饭')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (635, 71, N'买菜')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (636, 71, N'喝茶')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (637, 72, N'很远')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (638, 72, N'很近')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (639, 72, N'不太远')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (640, 73, N'妻子')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (641, 73, N'妹妹')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (642, 73, N'女儿')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (643, 74, N'天晴了')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (644, 74, N'下雨了')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (645, 74, N'下雪了')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (646, 75, N'朋友要来')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (647, 75, N'要去坐船')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (648, 75, N'要去上课')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (649, 76, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (650, 76, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (651, 76, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (652, 76, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (653, 76, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (654, 76, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (655, 77, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (656, 77, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (657, 77, N'C')
GO
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (658, 77, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (659, 77, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (660, 77, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (661, 78, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (662, 78, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (663, 78, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (664, 78, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (665, 78, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (666, 78, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (667, 79, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (668, 79, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (669, 79, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (670, 79, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (671, 79, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (672, 79, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (673, 80, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (674, 80, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (675, 80, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (676, 80, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (677, 80, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (678, 80, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (679, 81, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (680, 81, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (681, 81, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (682, 81, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (683, 81, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (684, 81, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (685, 82, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (686, 82, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (687, 82, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (688, 82, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (689, 82, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (690, 82, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (691, 83, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (692, 83, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (693, 83, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (694, 83, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (695, 83, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (696, 83, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (697, 84, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (698, 84, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (699, 84, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (700, 84, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (701, 84, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (702, 84, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (703, 85, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (704, 85, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (705, 85, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (706, 85, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (707, 85, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (708, 85, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (709, 86, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (710, 86, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (711, 87, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (712, 87, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (713, 88, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (714, 88, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (715, 89, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (716, 89, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (717, 90, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (718, 90, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (719, 91, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (720, 91, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (721, 91, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (722, 91, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (723, 91, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (724, 91, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (725, 92, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (726, 92, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (727, 92, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (728, 92, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (729, 92, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (730, 92, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (731, 93, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (732, 93, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (733, 93, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (734, 93, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (735, 93, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (736, 93, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (737, 94, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (738, 94, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (739, 94, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (740, 94, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (741, 94, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (742, 94, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (743, 95, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (744, 95, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (745, 95, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (746, 95, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (747, 95, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (748, 95, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (749, 96, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (750, 96, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (751, 96, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (752, 96, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (753, 96, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (754, 96, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (755, 97, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (756, 97, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (757, 97, N'C')
GO
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (758, 97, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (759, 97, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (760, 97, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (761, 98, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (762, 98, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (763, 98, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (764, 98, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (765, 98, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (766, 98, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (767, 99, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (768, 99, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (769, 99, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (770, 99, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (771, 99, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (772, 99, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (773, 100, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (774, 100, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (775, 100, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (776, 100, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (777, 100, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (778, 100, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (779, 251, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (780, 251, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (781, 251, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (782, 251, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (783, 251, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (784, 251, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (785, 252, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (786, 252, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (787, 252, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (788, 252, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (789, 252, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (790, 252, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (791, 253, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (792, 253, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (793, 253, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (794, 253, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (795, 253, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (796, 253, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (797, 254, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (798, 254, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (799, 254, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (800, 254, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (801, 254, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (802, 254, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (803, 255, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (804, 255, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (805, 255, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (806, 255, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (807, 255, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (808, 255, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (809, 256, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (810, 256, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (811, 256, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (812, 256, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (813, 256, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (814, 257, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (815, 257, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (816, 257, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (817, 257, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (818, 257, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (819, 258, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (820, 258, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (821, 258, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (822, 258, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (823, 258, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (824, 259, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (825, 259, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (826, 259, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (827, 259, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (828, 259, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (829, 260, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (830, 260, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (831, 260, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (832, 260, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (833, 260, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (834, 261, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (835, 261, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (836, 262, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (837, 262, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (838, 263, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (839, 263, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (840, 264, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (841, 264, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (842, 265, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (843, 265, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (844, 266, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (845, 266, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (846, 267, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (847, 267, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (848, 268, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (849, 268, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (850, 269, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (851, 269, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (852, 270, N'Yes')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (853, 270, N'No')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (854, 271, N'迟到了')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (855, 271, N'生病了')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (856, 271, N'生气了')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (857, 272, N'教室')
GO
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (858, 272, N'机场')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (859, 272, N'宾馆')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (860, 273, N'有两只')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (861, 273, N'爱跳舞')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (862, 273, N'是新来的')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (863, 274, N'一次')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (864, 274, N'两次')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (865, 274, N'三次')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (866, 275, N'商店')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (867, 275, N'书店')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (868, 275, N'学校东门')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (869, 276, N'汉语')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (870, 276, N'历史')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (871, 276, N'数学')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (872, 277, N'喜欢看球赛')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (873, 277, N'喜欢打篮球')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (874, 277, N'喜欢参加比赛')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (875, 278, N'买个新的')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (876, 278, N'买个贵的')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (877, 278, N'先借一个')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (878, 279, N'上网')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (879, 279, N'爬山')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (880, 279, N'游泳')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (881, 280, N'国外')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (882, 280, N'飞机上')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (883, 280, N'老地方')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (884, 281, N'习惯')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (885, 281, N'天气')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (886, 281, N'文化')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (887, 282, N'夫妻')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (888, 282, N'妈妈和儿子')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (889, 282, N'爸爸和女儿')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (890, 283, N'很矮')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (891, 283, N'很大')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (892, 283, N'黄色的')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (893, 284, N'多云')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (894, 284, N'很热')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (895, 284, N'更冷')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (896, 285, N'邻居的')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (897, 285, N'客人的')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (898, 285, N'校长的')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (899, 286, N'夏天来了')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (900, 286, N'月亮出来了')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (901, 286, N'天黑得早了')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (902, 287, N'在二层')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (903, 287, N'影响不大')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (904, 287, N'已经结束了')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (905, 288, N'不要离开')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (906, 288, N'给她写信')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (907, 288, N'有更好的成绩')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (908, 289, N'衬衫里')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (909, 289, N'洗手间')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (910, 289, N'椅子上')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (911, 290, N'在洗澡')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (912, 290, N'在睡觉')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (913, 290, N'在看新闻')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (914, 291, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (915, 291, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (916, 291, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (917, 291, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (918, 291, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (919, 291, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (920, 292, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (921, 292, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (922, 292, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (923, 292, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (924, 292, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (925, 292, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (926, 293, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (927, 293, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (928, 293, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (929, 293, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (930, 293, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (931, 293, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (932, 294, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (933, 294, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (934, 294, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (935, 294, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (936, 294, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (937, 294, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (938, 295, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (939, 295, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (940, 295, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (941, 295, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (942, 295, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (943, 295, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (944, 296, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (945, 296, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (946, 296, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (947, 296, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (948, 296, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (949, 296, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (950, 297, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (951, 297, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (952, 297, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (953, 297, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (954, 297, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (955, 297, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (956, 298, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (957, 298, N'B')
GO
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (958, 298, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (959, 298, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (960, 298, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (961, 298, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (962, 299, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (963, 299, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (964, 299, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (965, 299, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (966, 299, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (967, 299, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (968, 300, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (969, 300, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (970, 300, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (971, 300, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (972, 300, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (973, 300, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (974, 301, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (975, 301, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (976, 301, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (977, 301, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (978, 301, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (979, 301, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (980, 302, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (981, 302, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (982, 302, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (983, 302, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (984, 302, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (985, 302, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (986, 303, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (987, 303, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (988, 303, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (989, 303, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (990, 303, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (991, 303, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (992, 304, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (993, 304, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (994, 304, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (995, 304, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (996, 304, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (997, 304, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (998, 305, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (999, 305, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1000, 305, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1001, 305, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1002, 305, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1003, 305, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1004, 306, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1005, 306, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1006, 306, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1007, 306, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1008, 306, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1009, 306, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1010, 307, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1011, 307, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1012, 307, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1013, 307, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1014, 307, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1015, 307, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1016, 308, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1017, 308, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1018, 308, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1019, 308, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1020, 308, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1021, 308, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1022, 309, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1023, 309, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1024, 309, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1025, 309, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1026, 309, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1027, 309, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1028, 310, N'A')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1029, 310, N'B')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1030, 310, N'C')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1031, 310, N'D')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1032, 310, N'E')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1033, 310, N'F')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1034, 311, N'要努力工作')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1035, 311, N'明天会更好')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1036, 311, N'时间过得太快')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1037, 312, N'老师')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1038, 312, N'学生')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1039, 312, N'经理')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1040, 313, N'没有花园')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1041, 313, N'房间很大')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1042, 313, N'离河很近')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1043, 314, N'没复习')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1044, 314, N'迟到了')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1045, 314, N'忘了关门')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1046, 315, N'上海变化很大')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1047, 315, N'现在是春节')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1048, 315, N'上海人很热情')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1049, 316, N'以前的同事')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1050, 316, N'以前的丈夫')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1051, 316, N'以前的男朋友')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1052, 317, N'他回到家了')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1053, 317, N'他正在喝咖啡')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1054, 317, N'咖啡馆在公园旁边')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1055, 318, N'很年轻')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1056, 318, N'课讲得好')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1057, 318, N'对学生要求高')
GO
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1058, 319, N'水果')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1059, 319, N'鸡蛋')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1060, 319, N'果汁')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1061, 320, N'头发很长')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1062, 320, N'不像妈妈')
INSERT [dbo].[Answers] ([AnswerID], [QuestionID], [AnswerText]) VALUES (1063, 320, N'鼻子像爸爸')
SET IDENTITY_INSERT [dbo].[Answers] OFF
GO
SET IDENTITY_INSERT [dbo].[Exams] ON 

INSERT [dbo].[Exams] ([ExamID], [Title], [Description], [Date], [Listening]) VALUES (1, N'Test1', N'HSK1', CAST(N'2024-01-12' AS Date), N'\Dictionary\Exam\HSK1\1.wav')
INSERT [dbo].[Exams] ([ExamID], [Title], [Description], [Date], [Listening]) VALUES (2, N'Test1', N'HSK2', CAST(N'2024-01-12' AS Date), N'\Dictionary\Exam\HSK2\H2.wav')
INSERT [dbo].[Exams] ([ExamID], [Title], [Description], [Date], [Listening]) VALUES (3, N'Test1', N'HSK3', CAST(N'2024-01-12' AS Date), N'\Dictionary\Exam\HSK3\H3.wav')
INSERT [dbo].[Exams] ([ExamID], [Title], [Description], [Date], [Listening]) VALUES (4, N'Test1', N'HSK4', CAST(N'2024-01-12' AS Date), N'\Dictionary\Exam\HSK4\H4.wav')
INSERT [dbo].[Exams] ([ExamID], [Title], [Description], [Date], [Listening]) VALUES (5, N'Test1', N'HSK5', CAST(N'2024-01-12' AS Date), N'\Dictionary\Exam\HSK5\H5.wav')
INSERT [dbo].[Exams] ([ExamID], [Title], [Description], [Date], [Listening]) VALUES (6, N'Test1', N'HSK6', CAST(N'2024-01-12' AS Date), N'\Dictionary\Exam\HSK6\H6.wav')
SET IDENTITY_INSERT [dbo].[Exams] OFF
GO
INSERT [dbo].[Practice] ([Word], [Mean], [Dificult]) VALUES (N'去', N'✚[qù] Hán Việt: KHỨ
1. rời bỏ
2. mất đi; không còn
3. loại trừ; gạt bỏ
4. khoảng cách
5. năm ngoái; mùa trước (chỉ khoảng thời gian đã qua)
6. đi
7. hãy; cứ; phải (dùng trước một động từ khác, biểu thị sự cần làm)
8. đi (dùng sau kết cấu động tân, biểu thị đi làm một việc gì đó)
9. để; mà (dùng giữa kết cấu động từ (hoặc kết cấu giới từ) và động từ (hoặc kết cấu động từ), khi đó hành động thứ nhất là phương pháp, biện pháp , thái độ của hành động thứ hai, hành động thứ hai là mục đích của hành động thứ nhất)
10. rất; quá; lắm (dùng sau các hình dung tư ''大，多，远'' biểu thi số lượng nhiều)
11. khứ thanh; thanh tư (trong bốn thanh của tiếng phổ thông Trung Quốc)
12. diễn; đóng vai
13. dùng sau động từ biểu thị động tác dời xa người nói
14. (dùng sau động từ, biểu thị sự tiếp tục của động tác)
', 1)
INSERT [dbo].[Practice] ([Word], [Mean], [Dificult]) VALUES (N'游戏', N'✚[yóuxì] 
1. trò chơi; du hý
2. vui chơi; nô đùa; chơi đùa
', 2)
INSERT [dbo].[Practice] ([Word], [Mean], [Dificult]) VALUES (N'这么激动', N'kích động như vậy
', 4)
INSERT [dbo].[Practice] ([Word], [Mean], [Dificult]) VALUES (N'đệ', N'第
', 2)
INSERT [dbo].[Practice] ([Word], [Mean], [Dificult]) VALUES (N'过去', N'✚[guòqù] 
1. đã qua; quá khứ; trước đây
2. đi qua; qua
3. mất; chết; tạ thế
4. (dùng sau động từ biểu thị sử chuyển dịch từ nơi này sang nơi khác)
5. (dùng sau động từ biểu thị mặt trái hướng về mình)
6. (dùng sau động từ, biểu thị mất đi trạng thái bình thường)
7. (dùng sau động từ, biểu thị sự thông qua)
8. (dùng sau tính từ, biểu thị sự vượt qua, thường dùng với ''得'' hoặc ''不'')
', 2)
GO
SET IDENTITY_INSERT [dbo].[Questions] ON 

INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (1, 1, N'\Dictionary\Exam\HSK1\1.PNG', N'Yes')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (2, 1, N'\Dictionary\Exam\HSK1\2.PNG', N'No')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (3, 1, N'\Dictionary\Exam\HSK1\3.PNG', N'No')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (4, 1, N'\Dictionary\Exam\HSK1\4.PNG', N'No')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (5, 1, N'\Dictionary\Exam\HSK1\5.PNG', N'Yes')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (6, 1, N'\Dictionary\Exam\HSK1\6.PNG', N'A')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (7, 1, N'\Dictionary\Exam\HSK1\7.PNG', N'A')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (8, 1, N'\Dictionary\Exam\HSK1\8.PNG', N'C')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (9, 1, N'\Dictionary\Exam\HSK1\9.PNG', N'A')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (10, 1, N'\Dictionary\Exam\HSK1\10.PNG', N'C')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (11, 1, N'\Dictionary\Exam\HSK1\11-15.PNG', N'D')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (12, 1, N'\Dictionary\Exam\HSK1\11-15.PNG', N'B')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (13, 1, N'\Dictionary\Exam\HSK1\11-15.PNG', N'A')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (14, 1, N'\Dictionary\Exam\HSK1\11-15.PNG', N'E')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (15, 1, N'\Dictionary\Exam\HSK1\11-15.PNG', N'F')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (16, 1, N'', N'我的')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (17, 1, N'', N'星期五')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (18, 1, N'', N'50')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (19, 1, N'', N'杯子')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (20, 1, N'', N'很漂亮')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (21, 1, N'\Dictionary\Exam\HSK1\21.PNG', N'Yes')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (22, 1, N'\Dictionary\Exam\HSK1\22.PNG', N'Yes')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (23, 1, N'\Dictionary\Exam\HSK1\23.PNG', N'No')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (24, 1, N'\Dictionary\Exam\HSK1\24.PNG', N'No')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (25, 1, N'\Dictionary\Exam\HSK1\25.PNG', N'Yes')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (26, 1, N'\Dictionary\Exam\HSK1\26-30.PNG', N'D')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (27, 1, N'\Dictionary\Exam\HSK1\26-30.PNG', N'F')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (28, 1, N'\Dictionary\Exam\HSK1\26-30.PNG', N'C')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (29, 1, N'\Dictionary\Exam\HSK1\26-30.PNG', N'A')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (30, 1, N'\Dictionary\Exam\HSK1\26-30.PNG', N'B')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (31, 1, N'\Dictionary\Exam\HSK1\31-35.PNG', N'C')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (32, 1, N'\Dictionary\Exam\HSK1\31-35.PNG', N'D')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (33, 1, N'\Dictionary\Exam\HSK1\31-35.PNG', N'A')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (34, 1, N'\Dictionary\Exam\HSK1\31-35.PNG', N'B')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (35, 1, N'\Dictionary\Exam\HSK1\31-35.PNG', N'E')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (36, 1, N'\Dictionary\Exam\HSK1\36-40.PNG', N'F')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (37, 1, N'\Dictionary\Exam\HSK1\36-40.PNG', N'B')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (38, 1, N'\Dictionary\Exam\HSK1\36-40.PNG', N'E')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (39, 1, N'\Dictionary\Exam\HSK1\36-40.PNG', N'A')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (40, 1, N'\Dictionary\Exam\HSK1\36-40.PNG', N'C')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (41, 2, N'\Dictionary\Exam\HSK2\1.PNG', N'No')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (42, 2, N'\Dictionary\Exam\HSK2\2.PNG', N'Yes')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (43, 2, N'\Dictionary\Exam\HSK2\3.PNG', N'No')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (44, 2, N'\Dictionary\Exam\HSK2\4.PNG', N'Yes')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (45, 2, N'\Dictionary\Exam\HSK2\5.PNG', N'Yes')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (46, 2, N'\Dictionary\Exam\HSK2\6.PNG', N'Yes')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (47, 2, N'\Dictionary\Exam\HSK2\7.PNG', N'No')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (48, 2, N'\Dictionary\Exam\HSK2\8.PNG', N'No')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (49, 2, N'\Dictionary\Exam\HSK2\9.PNG', N'Yes')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (50, 2, N'\Dictionary\Exam\HSK2\10.PNG', N'No')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (51, 2, N'\Dictionary\Exam\HSK2\11-15.PNG', N'C')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (52, 2, N'\Dictionary\Exam\HSK2\11-15.PNG', N'B')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (53, 2, N'\Dictionary\Exam\HSK2\11-15.PNG', N'F')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (54, 2, N'\Dictionary\Exam\HSK2\11-15.PNG', N'A')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (55, 2, N'\Dictionary\Exam\HSK2\11-15.PNG', N'E')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (56, 2, N'\Dictionary\Exam\HSK2\16-20.PNG', N'E')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (57, 2, N'\Dictionary\Exam\HSK2\16-20.PNG', N'D')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (58, 2, N'\Dictionary\Exam\HSK2\16-20.PNG', N'B')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (59, 2, N'\Dictionary\Exam\HSK2\16-20.PNG', N'C')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (60, 2, N'\Dictionary\Exam\HSK2\16-20.PNG', N'A')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (61, 2, N'', N'看电视')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (62, 2, N'', N'饭店')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (63, 2, N'', N'2000多')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (64, 2, N'', N'2008年')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (65, 2, N'', N'看过了')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (66, 2, N'', N'鱼')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (67, 2, N'', N'机场')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (68, 2, N'', N'颜色好')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (69, 2, N'', N'8月 7')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (70, 2, N'', N'送牛奶的')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (71, 2, N'', N'吃饭')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (72, 2, N'', N'不太远')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (73, 2, N'', N'女儿')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (74, 2, N'', N'下雪了')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (75, 2, N'', N'朋友要来')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (76, 2, N'\Dictionary\Exam\HSK2\36-40.PNG', N'E')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (77, 2, N'\Dictionary\Exam\HSK2\36-40.PNG', N'A')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (78, 2, N'\Dictionary\Exam\HSK2\36-40.PNG', N'B')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (79, 2, N'\Dictionary\Exam\HSK2\36-40.PNG', N'F')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (80, 2, N'\Dictionary\Exam\HSK2\36-40.PNG', N'C')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (81, 2, N'\Dictionary\Exam\HSK2\41-45.PNG', N'D')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (82, 2, N'\Dictionary\Exam\HSK2\41-45.PNG', N'F')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (83, 2, N'\Dictionary\Exam\HSK2\41-45.PNG', N'B')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (84, 2, N'\Dictionary\Exam\HSK2\41-45.PNG', N'C')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (85, 2, N'\Dictionary\Exam\HSK2\41-45.PNG', N'A')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (86, 2, N'我知道他的名字，她姓杨，叫杨笑笑，她姐姐是我同学。=》我认识杨笑笑的姐姐', N'Yes')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (87, 2, N'我的一个朋友正在找房子，希望住得离公司近一些，这样她每天早上会可以7点起床，比现在多睡一个小时 =》朋友现在每天7点起床', N'No')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (88, 2, N'我喜欢猫，但是丈夫不喜欢，所以到现在家里也没有猫。我希望有一天能有一个小猫 =》我丈夫想要一个小猫', N'No')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (89, 2, N'从我家到北京，坐火车就5个小时，比坐飞机便宜很多。所以，明天我准备坐火车去 =》我明天去北京', N'Yes')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (90, 2, N'李哥，赵老师让我告诉你，你的电影票买到了 =》 李哥正在看电影', N'No')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (91, 2, N'\Dictionary\Exam\HSK2\51-55.PNG', N'D')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (92, 2, N'\Dictionary\Exam\HSK2\51-55.PNG', N'A')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (93, 2, N'\Dictionary\Exam\HSK2\51-55.PNG', N'B')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (94, 2, N'\Dictionary\Exam\HSK2\51-55.PNG', N'F')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (95, 2, N'\Dictionary\Exam\HSK2\51-55.PNG', N'C')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (96, 2, N'\Dictionary\Exam\HSK2\56-60.PNG', N'B')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (97, 2, N'\Dictionary\Exam\HSK2\56-60.PNG', N'E')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (98, 2, N'\Dictionary\Exam\HSK2\56-60.PNG', N'C')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (99, 2, N'\Dictionary\Exam\HSK2\56-60.PNG', N'D')
GO
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (100, 2, N'\Dictionary\Exam\HSK2\56-60.PNG', N'A')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (251, 3, N'\Dictionary\Exam\HSK3\1-5.PNG', N'B')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (252, 3, N'\Dictionary\Exam\HSK3\1-5.PNG', N'C')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (253, 3, N'\Dictionary\Exam\HSK3\1-5.PNG', N'E')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (254, 3, N'\Dictionary\Exam\HSK3\1-5.PNG', N'F')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (255, 3, N'\Dictionary\Exam\HSK3\1-5.PNG', N'A')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (256, 3, N'\Dictionary\Exam\HSK3\6-10.PNG', N'B')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (257, 3, N'\Dictionary\Exam\HSK3\6-10.PNG', N'C')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (258, 3, N'\Dictionary\Exam\HSK3\6-10.PNG', N'A')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (259, 3, N'\Dictionary\Exam\HSK3\6-10.PNG', N'D')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (260, 3, N'\Dictionary\Exam\HSK3\6-10.PNG', N'E')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (261, 3, N'北京话和普通话是相同的', N'No')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (262, 3, N'会议室在 8 层。', N'Yes')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (263, 3, N'他已经到了。', N'No')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (264, 3, N'这是辆旧车。', N'Yes')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (265, 3, N'小孩子爱吃蛋糕。', N'Yes')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (266, 3, N'他在北京玩了很多地方。', N'No')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (267, 3, N'周明坐火车时喜欢看报纸。', N'Yes')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (268, 3, N'他喜欢音乐,也喜欢运动。', N'Yes')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (269, 3, N'她对自己的工作没兴趣。', N'No')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (270, 3, N'他们在买空调。', N'No')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (271, 3, N'', N'生病了')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (272, 3, N'', N'宾馆')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (273, 3, N'', N'是新来的')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (274, 3, N'', N'三次')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (275, 3, N'', N'书店')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (276, 3, N'', N'数学')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (277, 3, N'', N'喜欢看球赛')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (278, 3, N'', N'买个新的')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (279, 3, N'', N'爬山')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (280, 3, N'', N'老地方')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (281, 3, N'', N'天气')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (282, 3, N'', N'妈妈和儿子')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (283, 3, N'', N'很大')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (284, 3, N'', N'更冷')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (285, 3, N'', N'邻居的')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (286, 3, N'', N'天黑得早了')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (287, 3, N'', N'在二层')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (288, 3, N'', N'有更好的成绩')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (289, 3, N'', N'衬衫里')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (290, 3, N'', N'在洗澡')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (291, 3, N'\Dictionary\Exam\HSK3\41-45.PNG', N'C')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (292, 3, N'\Dictionary\Exam\HSK3\41-45.PNG', N'B')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (293, 3, N'\Dictionary\Exam\HSK3\41-45.PNG', N'A')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (294, 3, N'\Dictionary\Exam\HSK3\41-45.PNG', N'D')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (295, 3, N'\Dictionary\Exam\HSK3\41-45.PNG', N'F')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (296, 3, N'\Dictionary\Exam\HSK3\46-50.PNG', N'B')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (297, 3, N'\Dictionary\Exam\HSK3\46-50.PNG', N'D')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (298, 3, N'\Dictionary\Exam\HSK3\46-50.PNG', N'E')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (299, 3, N'\Dictionary\Exam\HSK3\46-50.PNG', N'A')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (300, 3, N'\Dictionary\Exam\HSK3\46-50.PNG', N'C')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (301, 3, N'\Dictionary\Exam\HSK3\51-55.PNG', N'F')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (302, 3, N'\Dictionary\Exam\HSK3\51-55.PNG', N'D')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (303, 3, N'\Dictionary\Exam\HSK3\51-55.PNG', N'C')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (304, 3, N'\Dictionary\Exam\HSK3\51-55.PNG', N'B')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (305, 3, N'\Dictionary\Exam\HSK3\51-55.PNG', N'A')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (306, 3, N'\Dictionary\Exam\HSK3\56-60.PNG', N'A')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (307, 3, N'\Dictionary\Exam\HSK3\56-60.PNG', N'B')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (308, 3, N'\Dictionary\Exam\HSK3\56-60.PNG', N'E')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (309, 3, N'\Dictionary\Exam\HSK3\56-60.PNG', N'F')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (310, 3, N'\Dictionary\Exam\HSK3\56-60.PNG', N'C')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (311, 3, N' 61.人们常说:今天工作不努力,明天努力找工作。
★ 这句话的意思主要是:', N'要努力工作')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (312, 3, N' 62.请大家把黑板上的这些词写在本子上,回家后用这些词语写一个小故事,别忘了,最少写 100 字。
★ 说话人最可能是做什么的?', N'老师')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (313, 3, N'63.我对这儿很满意,虽然没有花园,但是离河边很近,那里有草地,有大树, 还有鸟;虽然冬天天气很冷,但是空气新鲜,而且房间里一点儿也不冷。
★ 使他觉得满意的是:', N'离河很近')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (314, 3, N' 64.昨天晚上睡得太晚,今天起床时已经 8 点多了,我刷了牙,洗了脸,就出来了,差点儿忘了关门。到了公司,会议已经开始了。没办法,我只能站在外面等休息时间。
★ 他今天早上:', N'迟到了')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (315, 3, N' 65.我去年春节去过一次上海,今年再去的时候,发现那里的变化非常大。经过那条街道时,我几乎不认识了。
★ 根据这段话,可以知道:', N'上海变化很大')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (316, 3, N' 66.世界真的很小,我昨天才发现,你给小张介绍的男朋友是我妻子以前的同事。
★ 小张的男朋友是我妻子:', N'以前的同事')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (317, 3, N' 67.下班后,在路上遇到一个老同学。好久没见面,我们就在公司旁边那个咖啡馆里坐了坐,一边喝咖啡一边说了些过去的事,所以回来晚了。
★ 根据这段话,可以知道:', N'他回到家了')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (318, 3, N' 68.小刘是一位小学老师,教三年级的数学,他虽然很年轻,但是课讲得很好,同学们都很喜欢他。
★ 学生为什么喜欢刘老师?', N'课讲得好')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (319, 3, N' 69.今天 12 号了,晚上陈阿姨要来家里,家里有菜,有鱼,还有些羊肉,但是没有水果了,你去买些香蕉、葡萄吧,再买个西瓜?
★ 家里需要买什么?', N'水果')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (320, 3, N'70.有人问我长得像谁,这个问题不太好回答。家里人一般觉得我的鼻子和耳朵像我爸爸,眼睛像我妈妈。
★ 关于他,下面哪个是对的?', N'鼻子像爸爸')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (321, 3, N'71.弟弟/笑了/高兴地', N' 弟弟高兴地笑了')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (322, 3, N' 72.简单/上午的考试/比较', N'上午的考试比较简单')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (323, 3, N' 73.越来越好/变得/这个城市的环境/了', N' 这个城市的环境变得越来越好了')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (324, 3, N' 74.送给他/那位/医生/一个礼物', N' 那位医生送给他一个礼物')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (325, 3, N' 75.其他班的成绩/有/也/很大/提高', N' 其他班的成绩也有很大提高')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (326, 3, N'76.医院离这儿很远,我们坐(chū)租车去吧。', N'出')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (327, 3, N' 77.一(yuán)是 10 角,一角是 10 分。', N'元')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (328, 3, N' 78.我不认识他,你知道他姓什么、(jiào)什么吗?', N'叫')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (329, 3, N' 79.(Zhōng)间穿红裙子的一定是他妹妹。', N'中')
INSERT [dbo].[Questions] ([QuestionID], [ExamID], [QuestionText], [CorrectAnswer]) VALUES (330, 3, N' 80.我已经饱了,不想吃(mǐ)饭了。', N'米')
SET IDENTITY_INSERT [dbo].[Questions] OFF
GO
SET IDENTITY_INSERT [dbo].[Results] ON 

INSERT [dbo].[Results] ([ResultID], [ExamID], [Score], [DateTaken]) VALUES (84, 3, 2, CAST(N'2024-03-19T14:01:16.913' AS DateTime))
SET IDENTITY_INSERT [dbo].[Results] OFF
GO
SET IDENTITY_INSERT [dbo].[StudentResponses] ON 

INSERT [dbo].[StudentResponses] ([ResponseID], [ResultID], [QuestionID], [SelectedAnswer]) VALUES (126, 84, 251, N'B')
INSERT [dbo].[StudentResponses] ([ResponseID], [ResultID], [QuestionID], [SelectedAnswer]) VALUES (127, 84, 252, N'C')
SET IDENTITY_INSERT [dbo].[StudentResponses] OFF
GO
ALTER TABLE [dbo].[Answers]  WITH CHECK ADD FOREIGN KEY([QuestionID])
REFERENCES [dbo].[Questions] ([QuestionID])
GO
ALTER TABLE [dbo].[Questions]  WITH CHECK ADD FOREIGN KEY([ExamID])
REFERENCES [dbo].[Exams] ([ExamID])
GO
ALTER TABLE [dbo].[Results]  WITH CHECK ADD FOREIGN KEY([ExamID])
REFERENCES [dbo].[Exams] ([ExamID])
GO
ALTER TABLE [dbo].[StudentResponses]  WITH CHECK ADD FOREIGN KEY([QuestionID])
REFERENCES [dbo].[Questions] ([QuestionID])
GO
ALTER TABLE [dbo].[StudentResponses]  WITH CHECK ADD FOREIGN KEY([ResultID])
REFERENCES [dbo].[Results] ([ResultID])
GO
USE [master]
GO
ALTER DATABASE [Dictionary] SET  READ_WRITE 
GO
